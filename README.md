# Model Viewer

This is a standalone application for use by artists and [gameplay](https://en.wikipedia.org/wiki/Gameplay) developers working with the **[Ghoti](https://gitlab.com/psahani/ghoti)** game engine. It allows the user to see how a given [3D model](https://en.wikipedia.org/wiki/3D_modeling) will appear in the engine/game. It also serves as the primary way to interact with the engine's custom, end-to-end [art/asset pipeline](https://en.wikipedia.org/wiki/Art_pipeline), so it automatically exports model data as well as any [HDR](https://en.wikipedia.org/wiki/High_dynamic_range) [environment maps](https://en.wikipedia.org/wiki/Reflection_mapping) to optimized [binary formats](https://en.wikipedia.org/wiki/Binary_file) compatible with the engine.

*Examining a [3D PBR model](https://artisaverb.info/PBT.html) by Andrew Maximov in an [HDR snowy environment](https://polyhaven.com/a/snowy_park_01) by Oliksiy Yakovlyev:*

![](videos/cerberus.mp4)

## Features

- An orbital camera view with a simple grid for scale
- A toggleable [wireframe](https://en.wikipedia.org/wiki/Wire-frame_model) mode for visualization and debugging
- Support for playback of and switching between animations
- Exporting of [FBX](https://en.wikipedia.org/wiki/FBX) model data to optimized binary file formats compatible with the engine
- A [PBR](https://en.wikipedia.org/wiki/Physically_based_rendering) material workflow with support for [albedo](https://en.wikipedia.org/wiki/Albedo), [normal](https://en.wikipedia.org/wiki/Normal_mapping), [height (parallax)](https://en.wikipedia.org/wiki/Parallax_mapping), metallic, roughness, and [ambient occlusion](https://en.wikipedia.org/wiki/Ambient_occlusion) [maps](https://en.wikipedia.org/wiki/Texture_mapping)
- Adherence to a detailed [specification](https://en.wikipedia.org/wiki/Specification_(technical_standard)) (a set of [naming conventions](https://en.wikipedia.org/wiki/Naming_convention_(programming))) for preparing [meshes](https://en.wikipedia.org/wiki/Polygon_mesh), materials ([UV maps](https://en.wikipedia.org/wiki/UV_mapping)), and [collision geometry](https://en.wikipedia.org/wiki/Collision_detection#Bounding_volumes) before being imported/exported as a part of the asset pipeline
- [Convolution](https://en.wikipedia.org/wiki/Convolution) of HDR environment maps to generate [pre-computed](https://en.wikipedia.org/wiki/Precomputation) [irradiance](https://en.wikipedia.org/wiki/Irradiance) maps for sampling [diffuse reflections](https://en.wikipedia.org/wiki/Diffuse_reflection)
- Pre-filtering of HDR environment maps through convolution at 5 different [mipmap](https://en.wikipedia.org/wiki/Mipmap) (roughness) levels for use in computing [specular reflections](https://en.wikipedia.org/wiki/Specular_reflection)
- Generation of a [BRDF](https://en.wikipedia.org/wiki/Bidirectional_reflectance_distribution_function) integration map in the form of a 2D LUT (lookup texture) for specular [IBL](https://en.wikipedia.org/wiki/Image-based_lighting)

*Viewing an animated 3D model of a dragon:*

![](videos/animations.mp4)

## Usage

The FBX file to be viewed should be placed in the `model/` folder prior to running the application. Any materials needed by the model should be placed in the `materials/` folder, while any masks should be placed in the `masks/` folder. An environment map in the [Radiance HDR](https://en.wikipedia.org/wiki/RGBE_image_format) file format should be placed into the `cubemaps/` folder before running the application, and the `graphics.pbr.ibl.cubemap.resolution` property in the `viewer.json` config file should be adjusted accordingly.

When running the app, a 2D BRDF LUT texture called `brdf_lut.hdr` will automatically be generated in the working directory.

### Exporting

#### Model

When the application is started, the FBX file is automatically exported into binary formats that can be used by the game engine. A folder with the same name as the FBX file will be created in the `model/` folder. If this folder already exists from a prior run, the material data from the previous JSON asset file will be used. The folder will contain an `asset/` folder which will contain files containing mesh, animation, and material information. There will also be an `entity/` folder which will contain an entity with the relevant components for the model. The `entity/` folder will also contain entities for any collision primitives or skeleton joints that may be present in the model.

#### Cubemap

Once the app is launched, the environment map will automatically be exported into a folder with the same name as the HDR file. The folder will contain three separate subfolders containing pre-computed cubemap, irradiance, and pre-filtered textures.

### Controls

| Binding             | Action                                        |
|:--------------------:|:---------------------------------------------:|
| Escape              | Exit the application                          |
| Left Click + Drag   | Rotate the camera                             |
| Middle Click + Drag | Pan the camera                                |
| Right Click + Drag  | Raise/lower the camera                        |
| Mouse Scroll        | Zoom the camera in/out                        |
| Q (Held)            | Speed up scrolling and movement of the camera |
| R                   | Reload material data                          |
| F                   | Toggle between fullscreen and windowed mode   |
| W                   | Toggle wireframe                              |
| C                   | Toggle collision primitives                   |
| L                   | Toggle light                                  |
| G                   | Toggle grid                                   |
| P                   | Pause/resume animation                        |
| Left                | Switch to previous animation                  |
| Right               | Switch to next animation                      |
| Up                  | Speed up animation                            |
| Down                | Slow down animation                           |
| O                   | Restart animation                             |
| I                   | Reverse animation                             |

## Releases

Compiled binaries for both Linux and Windows are available [here](https://gitlab.com/psahani/model-viewer/-/releases/).

## Development

### Useful Tools

- [LLDB](https://lldb.llvm.org/) - an open-source native debugger
- [Valgrind](https://valgrind.org/) - an open-source CLI tool which can be utilized for tracing memory leaks as well as profiling
- [RenderDoc](https://renderdoc.org/) - an invaluable open-source graphics debugger

### Building

#### Linux

Since care has been taken to include redistributable binaries of all necessary libraries in the `lib/` folder, all that should be needed to build the application is to have [Clang](https://clang.llvm.org/) and [Make](https://www.gnu.org/software/make/) installed before running this single command:

```sh
$ make rebuild
```

The resulting binary will be placed in the `build/` folder by default.

Various other makefile targets are also available including `run` (runs the app), `release` (compiles a release version of the app into the `release/` folder), `debug` (launches LLDB along with the app), and `leakcheck` (launches the app with Valgrind).

#### Windows

Building for Windows is only supported through [cross-compilation](https://en.wikipedia.org/wiki/Cross_compiler) from a Linux host using the [MinGW](https://www.mingw-w64.org/) compatibility environment. Redistributables are located in the `winlib/` folder. Assuming the `x86_64-w64-mingw32-clang` binary is available on the [system path](https://en.wikipedia.org/wiki/PATH_(variable)), simply run this command:

```sh
$ make windows
```

Release builds are also possible by setting the appropriate [environment variable](https://en.wikipedia.org/wiki/Environment_variable):

```sh
$ WINDOWS=true make release
```

The executable can be run on Linux using the [Wine](https://www.winehq.org/) compatibility layer if desired:

```sh
$ make wine
```

## Theory

This application serves as an interface to the asset pipeline, so it pre-processes HDR environment maps for use with the engine. The pre-computation necessary for feasibly achieving IBL in real-time is split into a diffuse and a specular part, corresponding to two components of the overall [reflectance equation](https://en.wikipedia.org/wiki/Rendering_equation):

![](images/split_reflectance_equation.png)

### Diffuse

![](images/brdf_diffuse.png)

#### Irradiance

The diffuse term is solved by sampling the HDR environment map using [spherical coordinates](https://en.wikipedia.org/wiki/Spherical_coordinate_system):

![](images/spherical_coordinates.png)

Convolution of the diffuse term yields the following equation for diffuse irradiance:

![](images/brdf_diffuse_convolution.png)

The [double integral](https://en.wikipedia.org/wiki/Multiple_integral) is [approximated discretely](https://en.wikipedia.org/wiki/Discretization) as follows:

![](images/discrete_brdf_diffuse_convolution.png)

This computation results in the irradiance map (*right*) for a given HDR environment map (*left*):

![](images/irradiance_map.png)

### Specular

The specular term is split into two separate integrals using what is called the split sum approximation (detailed [here](https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf)):

![](images/split_sum_approximation.png)

where the specular BRDF is

![](images/brdf_specular.png)

Both sides are pre-computed separately and then combined to solve the whole of the equation.

#### Pre-Filter

The left-hand side of the split sum is solved using a [numerical method](https://en.wikipedia.org/wiki/Numerical_method) called [Monte Carlo integration](https://en.wikipedia.org/wiki/Monte_Carlo_integration). The method states that for a large enough *N* [random samples](https://en.wikipedia.org/wiki/Simple_random_sample), the approximate solution of an integral as a finite sum [converges](https://en.wikipedia.org/wiki/Convergent_series) to the true solution:

![](images/monte_carlo_integration.png)

The [law of large numbers](https://en.wikipedia.org/wiki/Law_of_large_numbers) allows this to work mathematically, and the method allows one to practically solve functions with a theoretically infinite number of elements in its [domain](https://en.wikipedia.org/wiki/Domain_of_a_function). The [probability density function](https://en.wikipedia.org/wiki/Probability_density_function) used here is based on the [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution).

Furthermore, using a [low-discrepancy sequence](https://en.wikipedia.org/wiki/Low-discrepancy_sequence) (one resembling a [uniform distribution](https://en.wikipedia.org/wiki/Continuous_uniform_distribution), a [Hammersley sequence](https://en.wikipedia.org/wiki/Low-discrepancy_sequence#Hammersley_set) based on the [Van der Corput sequence](https://en.wikipedia.org/wiki/Van_der_Corput_sequence) in this case) rather than a naïve [pseudorandom](https://en.wikipedia.org/wiki/Pseudorandom_number_generator) one can increase the [rate of convergence](https://en.wikipedia.org/wiki/Rate_of_convergence) of the sum:

![](images/low_discrepancy_sequence.png)

This is called [Quasi-Monte Carlo integration](https://en.wikipedia.org/wiki/Quasi-Monte_Carlo_method). Combining this with a [biased estimator](https://en.wikipedia.org/wiki/Bias_of_an_estimator) (this introduces an error which is acceptably unnoticeable visually in the final result) through [importance sampling](https://en.wikipedia.org/wiki/Importance_sampling), with sample vectors biased towards ones lying within the specular lobe (the spatial region containing most of the reflected light) depending on the roughness of a given surface, allows for a drastically smaller number of samples to be sufficient for satisfactorily quick and accurate convergence:

![](images/specular_lobe.png)

This convolution is computed 5 times corresponding to 5 different material roughness values. These results are stored as mipmap levels in a pre-filtered map for a given HDR environment map as such:

![](images/pre_filtered_map.png)

#### BRDF

The right-hand side of the split sum is manipulated by factoring out the [Fresnel term](https://en.wikipedia.org/wiki/Fresnel_equations) from the BRDF:

![](images/brdf_specular_convolution.png)

With application of the [Fresnel-Schlick approximation](https://en.wikipedia.org/wiki/Schlick's_approximation) and further derivation, the final specular BRDF as a split sum is as follows:

![](images/split_brdf_specular.png)

Through convolution, the BRDF LUT is generated:

![](images/brdf_lut.png)

## Implementation Notes

The code for much of the processing (importing/exporting) of 3D model data actually resides separately in the [model utility](https://gitlab.com/psahani/model-utility) repository. Additionally, the `convolute_cubemap.frag`, `prefilter_cubemap.frag`, and `brdf_lut.frag` shaders in the `resources/shaders/` folder contain most of the nontrivial GPU graphics code for processing HDR environment maps into formats more amenable to real-time computations necessary for IBL.