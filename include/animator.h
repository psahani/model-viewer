#pragma once

#include "definitions.h"

void initializeAnimator(void);
void adjustAnimatorSpeed(real32 deltaSpeed);
void restartAnimator(void);
void reverseAnimator(void);
void toggleAnimatorPaused(void);
void switchAnimation(bool backwards);
void updateAnimator(void);