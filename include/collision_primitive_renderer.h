#pragma once

#include "definitions.h"
#include "renderer_types.h"

int32 initializeCollisionPrimitiveRenderer(void);
int32 drawCollisionPrimitives(Camera *camera);
void shutdownCollisionPrimitiveRenderer(void);