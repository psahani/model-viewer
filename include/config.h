#pragma once

#include "definitions.h"

#include <kazmath/vec2.h>
#include <kazmath/vec3.h>

#define CONFIG_FILE "viewer"

typedef struct window_config_t {
	char *title;
	bool fullscreen;
	bool maximized;
	kmVec2 size;
	bool resizable;
	bool vsync;
	uint32 numMSAASamples;
} WindowConfig;

typedef struct graphics_config_t {
	kmVec3 backgroundColor;
	bool pbr;
	uint32 cubemapResolution;
	uint32 irradianceMapResolution;
	uint32 prefilterMapResolution;
	bool cubemapDebugMode;
	real32 cubemapDebugMipLevel;
} GraphicsConfig;

typedef struct viewer_config_t {
	kmVec3 gridColor;
	uint32 gridDimensions[2];
	real32 gridCellSize;
	kmVec3 wireframeColor;
	kmVec3 boxColor;
	kmVec3 sphereColor;
	kmVec3 capsuleColor;
} ViewerConfig;

typedef struct log_config_t {
	char *viewerFile;
} LogConfig;

typedef struct config_t {
	WindowConfig windowConfig;
	GraphicsConfig graphicsConfig;
	ViewerConfig viewerConfig;
	LogConfig logConfig;
} Config;

int32 loadConfig(void);
void freeConfig(void);