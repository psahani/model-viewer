#pragma once

#include "definitions.h"

#include <GL/glew.h>

typedef struct cubemap_t {
	UUID name;
	GLuint cubemapID;
	GLuint irradianceID;
	GLuint prefilterID;
} Cubemap;

void initializeCubemapExporter(void);
void shutdownCubemapExporter(void);