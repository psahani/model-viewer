#pragma once

#include "definitions.h"
#include "renderer_types.h"

int32 initializeCubemapRenderer(void);
int32 drawCubemap(Camera *camera);
void shutdownCubemapRenderer(void);