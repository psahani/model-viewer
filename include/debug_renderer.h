#pragma once

#include "definitions.h"
#include "renderer_types.h"

#include <kazmath/vec3.h>
#include <kazmath/quaternion.h>

#define MAX_DEBUG_LINE_COUNT 2048
#define MAX_DEBUG_VERTEX_COUNT MAX_DEBUG_LINE_COUNT * 2

int32 initializeDebugRenderer(void);
void drawLine(
	const kmVec3 *positionA,
	const kmVec3 *positionB,
	const kmVec3 *color
);
void drawTransform(Transform transform, real32 size);
void drawGrid(uint32 rows, uint32 columns, real32 cellSize, kmVec3 *color);
int32 drawDebugLines(Camera *camera);
void shutdownDebugRenderer(void);