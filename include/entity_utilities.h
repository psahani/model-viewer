#pragma once

#include "definitions.h"

#include <cjson/cJSON.h>

void addInteger(
	cJSON *component,
	const char *name,
	const char *dataType,
	int64 integer
);

void addRealNumber(
	cJSON *component,
	const char *name,
	const char *dataType,
	real64 number
);

void addFloatArray(
	cJSON *component,
	const char *name,
	uint32 numValues,
	const real32 *values
);

void addString(
	cJSON *component,
	const char *name,
	uint32 length,
	const char *string
);

void addBool(cJSON *component, const char *name, bool value);
void addUUID(cJSON *component, const char *name, UUID uuid);

void addTransform(
	cJSON *components,
	const real32 *positionValues,
	const real32 *rotationValues,
	const real32 *scaleValues,
	UUID *parent,
	UUID *firstChild,
	UUID *nextSibling
);

void getFloatArray(cJSON *array, uint32 numValues, real32 *values);