#pragma once

#include "definitions.h"

#include <GL/glew.h>
#include <GL/glu.h>

#include <GLFW/glfw3.h>

#include <kazmath/vec2.h>
#include <kazmath/vec4.h>

#define MAX_GUI_VERTEX_COUNT 512 * 1024
#define MAX_GUI_INDEX_COUNT 128 * 1024

int32 initializeGUI(void);
void updateGUI(GLFWwindow* window);
int32 drawGUI(kmVec2 scale, int32 width, int32 height);
void shutdownGUI(void);