#pragma once

#include "definitions.h"
#include "renderer_types.h"

#include <stdio.h>

#define MODEL_FOLDER "model"
#define COLLISION_PRIMITIVES_FOLDER_NAME "collision_primitives"

#define MESH_BINARY_FILE_VERSION 1

int32 importModel(bool export);
int32 reloadModel(void);
void freeResources(void);

char* getFullTextureFilename(const char *filename);
int32 loadMesh(Mesh *mesh, FILE *file);
void freeMesh(Mesh *mesh);