#pragma once

#include "definitions.h"

#include <GLFW/glfw3.h>

#define INPUT_SPEED (speedUpButtonHeldDown ? 20.0f : 1.0f)

#define X_ROTATION_SENSITIVITY 10.0f
#define Y_ROTATION_SENSITIVITY 10.0f
#define X_TRANSLATION_SENSITIVITY 20.0f * INPUT_SPEED
#define Y_TRANSLATION_SENSITIVITY 20.0f * INPUT_SPEED
#define Z_TRANSLATION_SENSITIVITY 20.0f * INPUT_SPEED
#define ZOOM_SENSITIVITY 200.0f * INPUT_SPEED
#define ANIMATION_SPEED_SENSITIVITY 0.1f

void initializeInput(GLFWwindow *window);