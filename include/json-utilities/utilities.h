#pragma once

#include "definitions.h"

#include "file_utilities.h"

int32 exportEntity(const char *filename, Log log);
int32 exportAsset(const char *filename, Log log);
int32 exportScene(const char *filename, Log log);