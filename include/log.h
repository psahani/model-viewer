#pragma once

#include "definitions.h"

#include "config.h"

#include <stdio.h>

internal FILE *logFile;

extern Config config;

#ifdef _DEBUG
#define LOG_FILE_NAME NULL
#define LOG(...) printf(__VA_ARGS__)
#else
#define LOG_FILE_NAME config.logConfig.viewerFile
#define LOG(...) logFile = fopen(LOG_FILE_NAME, "a"); \
				 fprintf(logFile, __VA_ARGS__); \
				 fclose(logFile)
#endif

void logFunction(const char *format, ...);