#pragma once

#include "definitions.h"

#include "file_utilities.h"

int32 exportMaterials(
	const char *filename,
	bool collisionPrimitivesOnly,
	Log log
);