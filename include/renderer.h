#pragma once

#include "definitions.h"
#include "renderer_types.h"

#include <kazmath/vec3.h>
#include <kazmath/quaternion.h>

int32 initializeRenderer(void);
int32 drawModel(Camera *camera, kmVec3 scale, bool wireframe, bool light);
void shutdownRenderer(void);