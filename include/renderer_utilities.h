#pragma once

#include "definitions.h"
#include "renderer_types.h"

#include <kazmath/mat3.h>
#include <kazmath/mat4.h>

#include <stdio.h>

int32 createShaderProgram(
	const char *vertexShader,
	const char *controlShader,
	const char *evaluationShader,
	const char *geometryShader,
	const char *fragmentShader,
	const char *computeShader,
	GLuint *program
);

int32 getUniform(
	GLuint program,
	char *name,
	UniformType type,
	Uniform *uniform
);

int32 setUniform(Uniform uniform, uint32 count, void *data);

bool checkGLExtension(const char *extension);
int32 logGLError(bool logNoError, const char *message, ...);

Transform createTransform(
	const kmVec3 *position,
	const kmQuaternion *rotation,
	const kmVec3 *scale
);

Transform createIdentityTransform(void);
void addChildToTransform(Transform *transform, Transform *child);
kmMat4 composeTransform(const Transform *transform);

void concatenateTransforms(
	Transform *outTransform,
	const Transform *transformA,
	const Transform *transformB
);

kmVec3 transformVertex(const kmVec3 *vertex, const Transform *transform);
Transform readTransform(FILE *file);
void freeTransform(Transform *transform);

int32 setCameraUniforms(
	Camera camera,
	Uniform *viewUniform,
	Uniform *projectionUniform
);