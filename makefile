PROJ = model-viewer

IDIRS = include vendor
SRCDIR = src

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj

_LIBDIRS = lib
LIBDIRS = $(foreach LIBDIR,$(_LIBDIRS),-L$(LIBDIR) -Wl,-rpath-link,$(LIBDIR))

CC = clang
CCDB = lldb
CFLAGS = $(foreach DIR,$(IDIRS),-I$(DIR))
DBFLAGS = -g -D_DEBUG -O0 -Wall
RELFLAGS = -O3

_LIBS = file-utilities json-utilities model-utility cjson glfw GLEW GLU GL kazmath m
LIBS = $(foreach LIB,$(_LIBS),-l$(LIB))

DEPS = $(shell find include -name *.h)
VENDORDEPS = $(shell find vendor -name *.h)

OBJ = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(shell find $(SRCDIR) -name *.c))

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) -c -o $@ $<

.PHONY: build

build : $(OBJ)
	$(CC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(LIBDIRS) -o $(BUILDDIR)/$(PROJ) $^ $(LIBS)

.PHONY: clean

clean:
	rm -rf release
	rm -rf $(BUILDDIR)
	mkdir -p $(OBJDIR)
	mkdir -p model/
	mkdir -p masks/
	mkdir -p materials/
	mkdir -p cubemaps/

.PHONY: rebuild

rebuild : clean build

.PHONY: run

run : build
	LD_LIBRARY_PATH=./lib $(BUILDDIR)/$(PROJ)

SUPPRESSIONS = $(PROJ).supp

.PHONY: leakcheck

leakcheck : build
	LD_LIBRARY_PATH=./lib valgrind --leak-check=full --track-origins=yes --suppressions=$(SUPPRESSIONS) $(BUILDDIR)/$(PROJ)

.PHONY: debug

debug : build
	LD_LIBRARY_PATH=./lib $(CCDB) $(BUILDDIR)/$(PROJ)

.PHONY: release

release : clean
	@make RELEASE=yes$(if $(WINDOWS), windows,)
	mkdir -p release/model/
	mkdir -p release/masks/
	mkdir -p release/cubemaps/
	find build/* -type f -not -path '*/obj/*' -exec cp {} release/ \;
	cp viewer.json release/
	cp -r materials/ release/
	cp -r resources/ release/
	cp -r collision_primitives/ release/
	cp cubemap.mesh release/
	cp README.md release/
	$(if $(WINDOWS),,mv release/$(PROJ) release/$(PROJ)-bin)
	$(if $(WINDOWS),,cp -r lib/ release/)
	$(if $(WINDOWS),,echo '#!/bin/bash' > release/$(PROJ) && echo 'LD_LIBRARY_PATH=./lib ./$(PROJ)-bin $$@' >> release/$(PROJ) && chmod +x release/$(PROJ))

WINCC = x86_64-w64-mingw32-clang
WINFLAGS = -I/usr/local/include -Wl,-subsystem,windows
_WINLIBS = file-utilities json-utilities model-utility cjson glfw3 glew32 glu32 opengl32 kazmath
WINLIBS = $(foreach LIB,$(_WINLIBS),-l$(LIB))

_WINLIBDIRS = winlib
WINLIBDIRS = $(foreach LIBDIR,$(_WINLIBDIRS),-L$(LIBDIR))

WINOBJ = $(patsubst %.o,%.obj,$(OBJ))

$(OBJDIR)/%.obj : $(SRCDIR)/%.c $(DEPS)
	$(WINCC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) -c -o $@ $<

.PHONY: windows

windows : $(WINOBJ)
	$(WINCC) $(CFLAGS) $(if $(RELEASE),$(RELFLAGS),$(DBFLAGS)) $(WINFLAGS) $(WINLIBDIRS) $(if $(LIBRARY),$(SHAREDFLAGS),) -o $(BUILDDIR)/$(PROJ).exe $^ $(WINLIBS)
	cp winlib/* $(BUILDDIR)/

.PHONY: wine

wine : windows
	wine $(BUILDDIR)/$(PROJ).exe