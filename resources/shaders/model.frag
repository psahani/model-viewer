#version 420 core

#define NUM_MATERIAL_COMPONENTS 7

const uint AMBIENT_OCCLUSION_COMPONENT = 0;
const uint BASE_COMPONENT = 1;
const uint EMISSIVE_COMPONENT = 2;
const uint HEIGHT_COMPONENT = 3;
const uint METALLIC_COMPONENT = 4;
const uint NORMAL_COMPONENT = 5;
const uint ROUGHNESS_COMPONENT = 6;

in vec4 fragColor;
in vec3 fragPosition;
in vec3 fragNormal;
in vec3 fragTangent;
in vec2 fragMaterialUV;
in vec2 fragMaskUV;

out vec4 color;

uniform bool materialActive[NUM_MATERIAL_COMPONENTS];
uniform sampler2D material[NUM_MATERIAL_COMPONENTS];
uniform vec3 materialValues[NUM_MATERIAL_COMPONENTS];

uniform bool opacityMaskActive;
uniform sampler2D opacityMask;

uniform bool useCustomColor;
uniform vec3 customColor;

uniform vec3 cameraPosition;
uniform vec3 cameraDirection;

uniform bool directionalLight;

bool isOpaque(vec2 uv);

mat3 createTBNMatrix(vec3 normal, vec3 tangent);
vec2 getMaterialUV(vec3 viewDirection, mat3 TBN);

vec3 getAlbedoTextureColor(vec2 uv);
float getHeightTextureColor(vec2 uv);
vec3 getNormalTextureColor(vec2 uv, mat3 TBN);

vec3 getDirectionalLightColor(vec3 normal, vec3 albedoTextureColor);

void main() {
	if (useCustomColor) {
		color = vec4(customColor, 1.0);
		return;
	}

	if (!isOpaque(fragMaskUV)) {
		discard;
	}

	vec3 viewDirection = normalize(cameraPosition - fragPosition);
	mat3 TBN = createTBNMatrix(fragNormal, fragTangent);

	vec2 materialUV = getMaterialUV(viewDirection, TBN);

	vec3 albedoTextureColor = getAlbedoTextureColor(materialUV);
	vec3 normalTextureColor = getNormalTextureColor(materialUV, TBN);

	vec3 finalColor = vec3(0.0);

	if (directionalLight) {
		finalColor += getDirectionalLightColor(
			normalTextureColor,
			albedoTextureColor
		);
	}

	color = vec4(finalColor, 1.0);
}

bool isOpaque(vec2 uv) {
	bool opaque = true;

	if (opacityMaskActive) {
		vec3 opacity = texture(opacityMask, uv).rgb;
		if (opacity == vec3(0.0)) {
			opaque = false;
		}
	}

	return opaque;
}

mat3 createTBNMatrix(vec3 normal, vec3 tangent) {
	tangent = normalize(tangent - dot(tangent, normal) * normal);
	vec3 bitangent = cross(tangent, normal);
	return mat3(tangent, bitangent, normal);
}

vec2 getMaterialUV(vec3 viewDirection, mat3 TBN) {
	if (!materialActive[HEIGHT_COMPONENT]) {
		return fragMaterialUV;
	}

	viewDirection = normalize(viewDirection * TBN);

	const vec2 layersRange = vec2(8, 32);
	float numLayers = mix(
		layersRange.y,
		layersRange.x,
		abs(dot(vec3(0.0, 0.0, 1.0), viewDirection))
	);

	vec2 P =
		(viewDirection.xy / viewDirection.z) *
		0.025 * materialValues[HEIGHT_COMPONENT].x;

	float layerDepth = 1.0 / numLayers;
	vec2 deltaUV = P / numLayers;

	float height = getHeightTextureColor(fragMaterialUV);

	vec2 uv; float l;
	for (uv = fragMaterialUV, l = 0.0; l < height; l += layerDepth) {
		uv -= deltaUV;
		height = getHeightTextureColor(uv);
	}

	vec2 lastUV = uv + deltaUV;

	vec2 depth = vec2(
		getHeightTextureColor(lastUV) - l + layerDepth,
		height - l
	);

	float weight = depth.y / (depth.y - depth.x);

	vec2 materialUV = lastUV * weight + uv * (1.0 - weight);

	if (materialValues[HEIGHT_COMPONENT].z == -1.0) {
		if (materialUV.x < 0.0 ||
			materialUV.x > 1.0 ||
			materialUV.y < 0.0 ||
			materialUV.y > 1.0) {
			discard;
		}
	}

	return materialUV;
}

vec3 getAlbedoTextureColor(vec2 uv) {
	vec3 albedoTextureColor = fragColor.rgb;
	if (materialActive[BASE_COMPONENT]) {
		albedoTextureColor = vec3(texture(material[BASE_COMPONENT], uv));
		albedoTextureColor *= materialValues[BASE_COMPONENT];
	}

	return albedoTextureColor;
}

float getHeightTextureColor(vec2 uv) {
	float height = 0.0;
	if (materialActive[HEIGHT_COMPONENT]) {
		int inverse = materialValues[HEIGHT_COMPONENT].y == 1.0 ? -1 : 1;
		height = inverse * texture(material[HEIGHT_COMPONENT], uv).r +
			materialValues[HEIGHT_COMPONENT].y;
	}

	return height;
}

vec3 getNormalTextureColor(vec2 uv, mat3 TBN) {
	vec3 normalTextureColor = fragNormal;
	if (materialActive[NORMAL_COMPONENT]) {
		normalTextureColor = texture(material[NORMAL_COMPONENT], uv).rgb;
		normalTextureColor = normalize(normalTextureColor * 2.0 - 1.0);
		normalTextureColor = normalize(vec3(
			normalTextureColor.rg * materialValues[NORMAL_COMPONENT].x,
			normalTextureColor.b)
		);

		normalTextureColor = normalize(TBN * normalTextureColor);
	}

	return normalTextureColor;
}

vec3 getDirectionalLightColor(vec3 normal, vec3 albedoTextureColor) {
	vec3 lightDirection = normalize(-cameraDirection);

	float diffuseValue = max(dot(normal, lightDirection), 0.0);

	vec3 ambientColor = 0.1 * albedoTextureColor;
	vec3 diffuseColor = vec3(1.0) * diffuseValue * albedoTextureColor;

	return ambientColor + diffuseColor;
}