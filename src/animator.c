#include "animator.h"
#include "renderer_types.h"
#include "importer.h"
#include "hash_map.h"
#include "math_utilities.h"
#include "renderer_utilities.h"

#include <string.h>

internal int32 currentAnimation;
internal real64 time;
internal real32 speed;
internal bool paused;

extern Model model;
extern real64 dt;

internal kmVec3 getCurrentPosition(Bone *bone);
internal kmQuaternion getCurrentRotation(Bone *bone);
internal kmVec3 getCurrentScale(Bone *bone);

void applyParentTransforms(Transform *transform);

void initializeAnimator(void) {
	currentAnimation = 0;
	time = 0.0;
	speed = 1.0f;
	paused = false;
}

void adjustAnimatorSpeed(real32 deltaSpeed) {
	speed += deltaSpeed;
}

void restartAnimator(void) {
	if (speed < 0.0) {
		time = model.animations[currentAnimation].duration;
	} else {
		time = 0.0;
	}
}

void reverseAnimator(void) {
	speed *= -1.0f;
}

void toggleAnimatorPaused(void) {
	paused = !paused;
}

void switchAnimation(bool backwards) {
	currentAnimation += backwards ? -1 : 1;
	if (currentAnimation < 0) {
		currentAnimation = model.numAnimations - 1;
	} else if (currentAnimation == model.numAnimations) {
		currentAnimation = 0;
	}

	time = 0.0;
	speed = 1.0f;
	paused = false;

	updateAnimator();
}

void updateAnimator(void) {
	if (paused || model.numAnimations == 0 || strlen(model.name.string) == 0) {
		return;
	}

	Skeleton *skeleton = &model.skeleton;

	Animation *animation = &model.animations[currentAnimation];

	for (uint32 i = 0; i < animation->numBones; i++) {
		Bone *bone = &animation->bones[i];

		Transform *jointTransform = hashMapGetData(
			skeleton->joints,
			&bone->name
		);

		if (jointTransform) {
			jointTransform->position = getCurrentPosition(bone);
			jointTransform->rotation = getCurrentRotation(bone);
			jointTransform->scale = getCurrentScale(bone);
		}
	}

	applyParentTransforms(model.skeleton.rootJoint);

	real64 deltaTime = speed * dt;
	time += deltaTime;

	while (time < 0.0) {
		time += animation->duration;
	}

	time = fmod(time, animation->duration);
}

kmVec3 getCurrentPosition(Bone *bone) {
	if (bone->numPositionKeyFrames == 1) {
		return bone->positionKeyFrames[0].value;
	}

	uint32 a = 0, b = 0;
	for (uint32 i = 0; i < bone->numPositionKeyFrames - 1; i++) {
		if (time <= bone->positionKeyFrames[i + 1].time) {
			a = i, b = i + 1;
			break;
		}
	}

	Vec3KeyFrame *keyFrameA = &bone->positionKeyFrames[a];
	Vec3KeyFrame *keyFrameB = &bone->positionKeyFrames[b];

	real64 dt = keyFrameB->time - keyFrameA->time;
	real64 t = (time - keyFrameA->time) / dt;

	kmVec3 position;
	kmVec3Lerp(&position, &keyFrameA->value, &keyFrameB->value, t);

	return position;
}

kmQuaternion getCurrentRotation(Bone *bone) {
	if (bone->numRotationKeyFrames == 1) {
		return bone->rotationKeyFrames[0].value;
	}

	uint32 a = 0, b = 0;
	for (uint32 i = 0; i < bone->numRotationKeyFrames - 1; i++) {
		if (time <= bone->rotationKeyFrames[i + 1].time) {
			a = i, b = i + 1;
			break;
		}
	}

	QuaternionKeyFrame *keyFrameA = &bone->rotationKeyFrames[a];
	QuaternionKeyFrame *keyFrameB = &bone->rotationKeyFrames[b];

	real64 dt = keyFrameB->time - keyFrameA->time;
	real64 t = (time - keyFrameA->time) / dt;

	kmQuaternion rotation;
	quaternionSlerp(&rotation, &keyFrameA->value, &keyFrameB->value, t);

	return rotation;
}

kmVec3 getCurrentScale(Bone *bone) {
	if (bone->numScaleKeyFrames == 1) {
		return bone->scaleKeyFrames[0].value;
	}

	uint32 a = 0, b = 0;
	for (uint32 i = 0; i < bone->numScaleKeyFrames - 1; i++) {
		if (time <= bone->scaleKeyFrames[i + 1].time) {
			a = i, b = i + 1;
			break;
		}
	}

	Vec3KeyFrame *keyFrameA = &bone->scaleKeyFrames[a];
	Vec3KeyFrame *keyFrameB = &bone->scaleKeyFrames[b];

	real64 dt = keyFrameB->time - keyFrameA->time;
	real64 t = (time - keyFrameA->time) / dt;

	kmVec3 scale;
	kmVec3Lerp(&scale, &keyFrameA->value, &keyFrameB->value, t);

	return scale;
}

void applyParentTransforms(Transform *transform) {
	Transform *parent = transform->parent;
	if (parent) {
		Transform globalParentTransform = createTransform(
			&parent->globalPosition,
			&parent->globalRotation,
			&parent->globalScale
		);

		Transform globalTransform;
		concatenateTransforms(
			&globalTransform,
			&globalParentTransform,
			transform
		);

		kmVec3Assign(&transform->globalPosition, &globalTransform.position);
		kmQuaternionAssign(
			&transform->globalRotation,
			&globalTransform.rotation
		);

		kmVec3Assign(&transform->globalScale, &globalTransform.scale);
	} else {
		kmVec3Assign(&transform->globalPosition, &transform->position);
		kmQuaternionAssign(&transform->globalRotation, &transform->rotation);
		kmVec3Assign(&transform->globalScale, &transform->scale);
	}

	for (uint32 i = 0; i < transform->numChildren; i++) {
		applyParentTransforms(transform->children[i]);
	}
}