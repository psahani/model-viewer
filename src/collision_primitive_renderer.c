#include "collision_primitive_renderer.h"
#include "renderer_utilities.h"
#include "entity_utilities.h"
#include "importer.h"
#include "file_utilities.h"
#include "hash_map.h"
#include "log.h"

#include <sys/stat.h>

#include <malloc.h>
#include <dirent.h>
#include <string.h>

#define COLLISION_PRIMITIVE_MESHES_FOLDER "collision_primitives"
#define BOX_MESH_NAME "box"
#define SPHERE_MESH_NAME "sphere"
#define CYLINDER_MESH_NAME "cylinder"
#define HEMISPHERE_MESH_NAME "hemisphere"

internal Mesh boxMesh;
internal Mesh sphereMesh;
internal Mesh cylinderMesh;
internal Mesh hemisphereMesh;

#define BOX_ENTITIES_FOLDER "boxes"
#define SPHERE_ENTITIES_FOLDER "spheres"
#define CAPSULE_ENTITIES_FOLDER "capsules"

typedef struct box_t {
	UUID joint;
	kmVec3 position;
	kmQuaternion rotation;
	kmVec3 extents;
} Box;

typedef struct sphere_t {
	UUID joint;
	kmVec3 position;
	real32 radius;
} Sphere;

typedef struct capsule_t {
	UUID joint;
	kmVec3 position;
	kmQuaternion rotation;
	real32 radius;
	real32 length;
} Capsule;

internal uint32 numBoxes;
internal Box *boxes;
internal uint32 numSpheres;
internal Sphere *spheres;
internal uint32 numCapsules;
internal Capsule *capsules;

#define VERTEX_SHADER_FILE "resources/shaders/model.vert"
#define FRAGMENT_SHADER_FILE "resources/shaders/model.frag"

internal GLuint shaderProgram;

internal Uniform modelUniform;
internal Uniform viewUniform;
internal Uniform projectionUniform;

internal Uniform hasAnimationsUniform;

internal Uniform useCustomColorUniform;
internal Uniform customColorUniform;

extern Model model;
extern HashMap entityJoints;

internal void drawCollisionPrimitive(
	const char *name,
	Mesh *mesh,
	Transform *transform,
	kmVec3 *color
);

internal int32 loadCollisionPrimitiveMesh(const char *name, Mesh *mesh);
internal void freeCollisionPrimitiveMesh(const char *name, Mesh *mesh);
internal void loadCollisionPrimitives(void);
internal void loadCollisionPrimitive(const char *filename);

int32 initializeCollisionPrimitiveRenderer(void) {
	LOG("Initializing collision primitive renderer...\n");

	if (loadCollisionPrimitiveMesh(BOX_MESH_NAME, &boxMesh) == -1) {
		return -1;
	}

	if (loadCollisionPrimitiveMesh(SPHERE_MESH_NAME, &sphereMesh) == -1) {
		return -1;
	}

	if (loadCollisionPrimitiveMesh(CYLINDER_MESH_NAME, &cylinderMesh) == -1) {
		return -1;
	}

	if (loadCollisionPrimitiveMesh(
		HEMISPHERE_MESH_NAME,
		&hemisphereMesh) == -1) {
		return -1;
	}

	loadCollisionPrimitives();

	if (loadCollisionPrimitiveMesh(
		HEMISPHERE_MESH_NAME,
		&hemisphereMesh) == -1) {
		return -1;
	}

	if (createShaderProgram(
		VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		FRAGMENT_SHADER_FILE,
		NULL,
		&shaderProgram) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "model", UNIFORM_MAT4, &modelUniform) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "view", UNIFORM_MAT4, &viewUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"projection",
		UNIFORM_MAT4,
		&projectionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"hasAnimations",
		UNIFORM_BOOL,
		&hasAnimationsUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"useCustomColor",
		UNIFORM_BOOL,
		&useCustomColorUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"customColor",
		UNIFORM_VEC3,
		&customColorUniform) == -1) {
		return -1;
	}

	LOG("Successfully initialized collision primitive renderer\n");

	return 0;
}

int32 drawCollisionPrimitives(Camera *camera) {
	if (numBoxes == 0 && numSpheres == 0 && numCapsules == 0) {
		return 0;
	}

	glUseProgram(shaderProgram);

	if (setCameraUniforms(*camera, &viewUniform, &projectionUniform) == 1) {
		return -1;
	}

	bool hasAnimations = false;
	setUniform(hasAnimationsUniform, 1, &hasAnimations);

	bool useCustomColor = true;
	setUniform(useCustomColorUniform, 1, &useCustomColor);

	for (uint32 i = 0; i < numBoxes; i++) {
		Box *box = &boxes[i];

		Transform transform = {};
		kmVec3Assign(&transform.position, &box->position);
		kmQuaternionAssign(&transform.rotation, &box->rotation);
		kmVec3Assign(&transform.scale, &box->extents);
		kmVec3Scale(&transform.scale, &transform.scale, 2.0f);

		if (strlen(box->joint.string) > 0) {
			Transform *jointTransform = hashMapGetData(
				model.skeleton.joints,
				&box->joint
			);

			Transform globalJointTransform = createTransform(
				&jointTransform->globalPosition,
				&jointTransform->globalRotation,
				&jointTransform->globalScale
			);

			concatenateTransforms(
				&transform,
				&globalJointTransform,
				&transform
			);
		}

		drawCollisionPrimitive(
			BOX_MESH_NAME,
			&boxMesh,
			&transform,
			&config.viewerConfig.boxColor
		);
	}

	for (uint32 i = 0; i < numSpheres; i++) {
		Sphere *sphere = &spheres[i];

		Transform transform = {};
		kmVec3Assign(&transform.position, &sphere->position);
		kmQuaternionIdentity(&transform.rotation);
		kmVec3Fill(
			&transform.scale,
			sphere->radius,
			sphere->radius,
			sphere->radius
		);

		kmVec3Scale(&transform.scale, &transform.scale, 2.0f);

		if (strlen(sphere->joint.string) > 0) {
			Transform *jointTransform = hashMapGetData(
				model.skeleton.joints,
				&sphere->joint
			);

			Transform globalJointTransform = createTransform(
				&jointTransform->globalPosition,
				&jointTransform->globalRotation,
				&jointTransform->globalScale
			);

			concatenateTransforms(
				&transform,
				&globalJointTransform,
				&transform
			);
		}

		drawCollisionPrimitive(
			SPHERE_MESH_NAME,
			&sphereMesh,
			&transform,
			&config.viewerConfig.sphereColor
		);
	}

	for (uint32 i = 0; i < numCapsules; i++) {
		Capsule *capsule = &capsules[i];

		Transform transform = {};
		kmVec3Assign(&transform.position, &capsule->position);
		kmQuaternionAssign(&transform.rotation, &capsule->rotation);
		kmVec3Fill(
			&transform.scale,
			capsule->radius * 2,
			capsule->radius * 2,
			capsule->length
		);

		if (strlen(capsule->joint.string) > 0) {
			Transform *jointTransform = hashMapGetData(
				model.skeleton.joints,
				&capsule->joint
			);

			Transform globalJointTransform = createTransform(
				&jointTransform->globalPosition,
				&jointTransform->globalRotation,
				&jointTransform->globalScale
			);

			concatenateTransforms(
				&transform,
				&globalJointTransform,
				&transform
			);
		}

		drawCollisionPrimitive(
			CYLINDER_MESH_NAME,
			&cylinderMesh,
			&transform,
			&config.viewerConfig.capsuleColor
		);

		kmVec3Fill(
			&transform.scale,
			capsule->radius * 2,
			capsule->radius * 2,
			capsule->radius * 2
		);

		Transform offsetTransform = transform;

		kmVec3 positionOffset;
		kmVec3Fill(&positionOffset, 0.0f, 0.0f, capsule->length / 2);

		kmQuaternionMultiplyVec3(
			&positionOffset,
			&offsetTransform.rotation,
			&positionOffset
		);

		kmVec3Add(
			&offsetTransform.position,
			&offsetTransform.position,
			&positionOffset
		);

		drawCollisionPrimitive(
			HEMISPHERE_MESH_NAME,
			&hemisphereMesh,
			&offsetTransform,
			&config.viewerConfig.capsuleColor
		);

		offsetTransform = transform;

		kmVec3Fill(&positionOffset, 0.0f, 0.0f, -capsule->length / 2);

		kmQuaternionMultiplyVec3(
			&positionOffset,
			&offsetTransform.rotation,
			&positionOffset
		);

		kmVec3Add(
			&offsetTransform.position,
			&offsetTransform.position,
			&positionOffset
		);

		offsetTransform.scale.z *= -1.0f;

		drawCollisionPrimitive(
			HEMISPHERE_MESH_NAME,
			&hemisphereMesh,
			&offsetTransform,
			&config.viewerConfig.capsuleColor
		);
	}

	glUseProgram(0);

	return 0;
}

void shutdownCollisionPrimitiveRenderer(void) {
	LOG("Shutting down collision primitive renderer...\n");

	freeCollisionPrimitiveMesh(BOX_MESH_NAME, &boxMesh);
	freeCollisionPrimitiveMesh(SPHERE_MESH_NAME, &sphereMesh);
	freeCollisionPrimitiveMesh(CYLINDER_MESH_NAME, &cylinderMesh);
	freeCollisionPrimitiveMesh(HEMISPHERE_MESH_NAME, &hemisphereMesh);

	free(boxes);
	free(spheres);
	free(capsules);

	glDeleteProgram(shaderProgram);

	LOG("Successfully shut down collision primitive renderer\n");
}

void drawCollisionPrimitive(
	const char *name,
	Mesh *mesh,
	Transform *transform,
	kmVec3 *color
) {
	kmMat4 worldMatrix = composeTransform(transform);
	setUniform(modelUniform, 1, &worldMatrix);

	glBindVertexArray(mesh->vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);

	for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
		glEnableVertexAttribArray(j);
	}

	setUniform(customColorUniform, 1, color);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	real32 lineWidth;
	glGetFloatv(GL_LINE_WIDTH, &lineWidth);
	glLineWidth(1.0f);

	glDisable(GL_CULL_FACE);

	glDrawElements(
		GL_TRIANGLES,
		mesh->numIndices,
		GL_UNSIGNED_INT,
		NULL
	);

	logGLError(false, "Failed to draw collision primitive (%s)", name);

	glLineWidth(lineWidth);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_CULL_FACE);

	for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
		glDisableVertexAttribArray(j);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

int32 loadCollisionPrimitiveMesh(const char *name, Mesh *mesh) {
	int32 error = 0;

	LOG("Loading model (%s)...\n", name);

	char *filename = getFullFilePath(
		name,
		"mesh",
		COLLISION_PRIMITIVE_MESHES_FOLDER
	);

	FILE *file = fopen(filename, "rb");

	if (file) {
		uint8 meshBinaryFileVersion;
		fread(&meshBinaryFileVersion, sizeof(uint8), 1, file);

		if (meshBinaryFileVersion < MESH_BINARY_FILE_VERSION) {
			LOG("WARNING: %s out of date\n", filename);
			error = -1;
		}
	} else {
		error = -1;
	}

	if (error != -1) {
		error = loadMesh(mesh, file);
		fclose(file);
	} else {
		LOG("Failed to open %s\n", filename);
		error = -1;
	}

	free(filename);

	if (error != -1) {
		LOG("Successfully loaded model (%s)\n", name);
	} else {
		LOG("Failed to load model (%s)\n", name);
	}

	return error;
}

void freeCollisionPrimitiveMesh(const char *name, Mesh *mesh) {
	LOG("Freeing model (%s)...\n", name);

	freeMesh(mesh);

	LOG("Successfully freed model (%s)\n", name);
}

void loadCollisionPrimitives(void) {
	numBoxes = 0;
	boxes = NULL;
	numSpheres = 0;
	spheres = NULL;
	numCapsules = 0;
	capsules = NULL;

	char *modelFolder = getFullFilePath(model.name.string, NULL, MODEL_FOLDER);
	char *entityFolder = getFullFilePath("entity", NULL, modelFolder);
	free(modelFolder);

	char *collisionPrimitivesFolder = getFullFilePath(
		COLLISION_PRIMITIVES_FOLDER_NAME,
		NULL,
		entityFolder
	);

	free(entityFolder);

	char *collisionPrimitiveFolders[3];
	collisionPrimitiveFolders[0] =  getFullFilePath(
		BOX_ENTITIES_FOLDER,
		NULL,
		collisionPrimitivesFolder
	);

	collisionPrimitiveFolders[1] =  getFullFilePath(
		SPHERE_ENTITIES_FOLDER,
		NULL,
		collisionPrimitivesFolder
	);

	collisionPrimitiveFolders[2] =  getFullFilePath(
		CAPSULE_ENTITIES_FOLDER,
		NULL,
		collisionPrimitivesFolder
	);

	free(collisionPrimitivesFolder);

	for (uint8 i = 0; i < 3; i++) {
		DIR *dir = opendir(collisionPrimitiveFolders[i]);
		if (dir) {
			struct dirent *dirEntry = readdir(dir);
			while (dirEntry) {
				if (strcmp(dirEntry->d_name, ".") &&
					strcmp(dirEntry->d_name, "..")) {
					char *folderPath = getFullFilePath(
						dirEntry->d_name,
						NULL,
						collisionPrimitiveFolders[i]
					);

					struct stat info;
					stat(folderPath, &info);

					if (S_ISREG(info.st_mode)) {
						char *filename = removeExtension(folderPath);
						loadCollisionPrimitive(filename);
						free(filename);
					}

					free(folderPath);
				}

				dirEntry = readdir(dir);
			}

			closedir(dir);
		}

		free(collisionPrimitiveFolders[i]);
	}
}

void loadCollisionPrimitive(const char *filename) {
	cJSON *json = loadJSON(filename, &logFunction);

	if (json) {
		cJSON *components = cJSON_GetObjectItem(
			json,
			"components"
		);

		cJSON *jsonBox = cJSON_GetObjectItem(components, "box");
		cJSON *jsonSphere = cJSON_GetObjectItem(
			components,
			"sphere"
		);

		cJSON *jsonCapsule = cJSON_GetObjectItem(
			components,
			"capsule"
		);

		UUID *joint = NULL;
		kmVec3 *position = NULL;
		kmQuaternion *rotation = NULL;

		if (jsonBox) {
			boxes = realloc(boxes, ++numBoxes * sizeof(Box));
			Box *box = &boxes[numBoxes - 1];

			joint = &box->joint;
			position = &box->position;
			rotation = &box->rotation;

			cJSON *jsonExtents = jsonBox->child;
			getFloatArray(
				jsonExtents->child->next,
				3,
				(real32*)&box->extents
			);
		} else if (jsonSphere) {
			spheres = realloc(
				spheres,
				++numSpheres * sizeof(Sphere)
			);

			Sphere *sphere = &spheres[numSpheres - 1];

			joint = &sphere->joint;
			position = &sphere->position;

			cJSON *jsonRadius = jsonSphere->child;
			sphere->radius =
				jsonRadius->child->next->valuedouble;
		} else if (jsonCapsule) {
			capsules = realloc(
				capsules,
				++numCapsules * sizeof(Capsule)
			);

			Capsule *capsule = &capsules[numCapsules - 1];

			joint = &capsule->joint;
			position = &capsule->position;
			rotation = &capsule->rotation;

			cJSON *jsonRadius = jsonCapsule->child;
			capsule->radius =
				jsonRadius->child->next->valuedouble;

			cJSON *jsonLength = jsonRadius->next;
			capsule->length =
				jsonLength->child->next->valuedouble;
		}

		cJSON *transform = cJSON_GetObjectItem(
			components,
			"transform"
		);

		cJSON *jsonPosition = transform->child;
		getFloatArray(
			jsonPosition->child->next,
			3,
			(real32*)position
		);

		cJSON *jsonRotation = jsonPosition->next;
		if (rotation) {
			getFloatArray(
				jsonRotation->child->next,
				4,
				(real32*)rotation
			);
		}

		cJSON *parent = jsonRotation->next->next;
		UUID jointID = stringToUUID(
			parent->child->next->valuestring
		);

		EntityJointTransform *jointTransform = hashMapGetData(
			entityJoints,
			&jointID
		);

		if (jointTransform) {
			*joint = stringToUUID(jointTransform->name.string);
		} else {
			*joint = stringToUUID("");
		}

		cJSON_Delete(json);
	}
}