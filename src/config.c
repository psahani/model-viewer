#include "config.h"
#include "log.h"

#include "file_utilities.h"

#include <cjson/cJSON.h>

#include <malloc.h>
#include <string.h>

#define GET_CONFIG_ITEM(name, key) \
cJSON *name = getConfigObject(json, key); \
if (name)

Config config;

internal void initializeDefaultConfig(void);
internal cJSON* getConfigObject(cJSON *json, const char *key);

internal bool cJSONToBool(cJSON *boolObject);

int32 loadConfig(void) {
	initializeDefaultConfig();

	cJSON *json = loadJSON(CONFIG_FILE, &logFunction);
	if (!json) {
		LOG("Failed to load %s.json\n", CONFIG_FILE);
		return -1;
	}

	// Window Config

	GET_CONFIG_ITEM(windowTitle, "window.title") {
		free(config.windowConfig.title);
		config.windowConfig.title = malloc(
			strlen(windowTitle->valuestring) + 1
		);

		strcpy(config.windowConfig.title, windowTitle->valuestring);
	}

	GET_CONFIG_ITEM(windowFullscreen, "window.fullscreen") {
		config.windowConfig.fullscreen = cJSONToBool(windowFullscreen);
	}

	GET_CONFIG_ITEM(windowMaximized, "window.maximized") {
		config.windowConfig.maximized = cJSONToBool(windowMaximized);
	}

	GET_CONFIG_ITEM(windowSize, "window.size") {
		real32 width = windowSize->child->valuedouble;
		real32 height = windowSize->child->next->valuedouble;

		if (width >= 1.0f && height >= 1.0f) {
			kmVec2Fill(&config.windowConfig.size, width, height);
		}
	}

	GET_CONFIG_ITEM(windowResizable, "window.resizable") {
		config.windowConfig.resizable = cJSONToBool(windowResizable);
	}

	GET_CONFIG_ITEM(windowVSYNC, "window.vsync") {
		config.windowConfig.vsync = cJSONToBool(windowVSYNC);
	}

	GET_CONFIG_ITEM(numMSAASamples, "graphics.msaa") {
		int32 numSamples = numMSAASamples->valueint;
		if (numSamples > 0 && numSamples <= 32) {
			config.windowConfig.numMSAASamples = numSamples;
		}
	}

	// Graphics Config

	GET_CONFIG_ITEM(graphicsBackgroundColor, "graphics.background_color") {
		real32 r = graphicsBackgroundColor->child->valuedouble;
		real32 g = graphicsBackgroundColor->child->next->valuedouble;
		real32 b = graphicsBackgroundColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.graphicsConfig.backgroundColor, r, g, b);
		}
	}

	GET_CONFIG_ITEM(pbr, "graphics.pbr.enabled") {
		config.graphicsConfig.pbr = cJSONToBool(pbr);
	}

	GET_CONFIG_ITEM(cubemapResolution, "graphics.pbr.ibl.cubemap.resolution") {
		int32 resolution = cubemapResolution->valueint;
		if (resolution > 0 && resolution % 16 == 0) {
			config.graphicsConfig.cubemapResolution = resolution;
			config.graphicsConfig.irradianceMapResolution = resolution / 16;
			config.graphicsConfig.prefilterMapResolution = resolution / 4;
		}
	}

	GET_CONFIG_ITEM(cubemapDebugMode, "graphics.pbr.ibl.cubemap.debug_mode") {
		config.graphicsConfig.cubemapDebugMode = cJSONToBool(cubemapDebugMode);
	}

	GET_CONFIG_ITEM(
		cubemapDebugMipLevel,
		"graphics.pbr.ibl.cubemap.debug_mip_level") {
		if (config.graphicsConfig.cubemapDebugMode) {
			config.graphicsConfig.cubemapDebugMipLevel =
				cubemapDebugMipLevel->valuedouble;
		}
	}

	// Viewer Config

	GET_CONFIG_ITEM(gridColor, "viewer.grid.color") {
		real32 r = gridColor->child->valuedouble;
		real32 g = gridColor->child->next->valuedouble;
		real32 b = gridColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.viewerConfig.gridColor, r, g, b);
		}
	}

	GET_CONFIG_ITEM(gridDimensions, "viewer.grid.dimensions") {
		int32 x = gridDimensions->child->valueint;
		int32 y = gridDimensions->child->next->valueint;

		if (x >= 1 && y >= 1) {
			config.viewerConfig.gridDimensions[0] = x;
			config.viewerConfig.gridDimensions[1] = y;
		}
	}

	GET_CONFIG_ITEM(gridCellSize, "viewer.grid.cell_size") {
		real32 cellSize = gridCellSize->valuedouble;
		if (cellSize > 0.0f) {
			config.viewerConfig.gridCellSize = cellSize;
		}
	}

	GET_CONFIG_ITEM(wireframeColor, "viewer.wireframe.color") {
		real32 r = wireframeColor->child->valuedouble;
		real32 g = wireframeColor->child->next->valuedouble;
		real32 b = wireframeColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.viewerConfig.wireframeColor, r, g, b);
		}
	}

	GET_CONFIG_ITEM(
		boxColor,
		"viewer.collision_primitives.colors.boxes"
	) {
		real32 r = boxColor->child->valuedouble;
		real32 g = boxColor->child->next->valuedouble;
		real32 b = boxColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.viewerConfig.boxColor, r, g, b);
		}
	}

	GET_CONFIG_ITEM(
		sphereColor,
		"viewer.collision_primitives.colors.spheres"
	) {
		real32 r = sphereColor->child->valuedouble;
		real32 g = sphereColor->child->next->valuedouble;
		real32 b = sphereColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.viewerConfig.sphereColor, r, g, b);
		}
	}

	GET_CONFIG_ITEM(
		capsuleColor,
		"viewer.collision_primitives.colors.capsules"
	) {
		real32 r = capsuleColor->child->valuedouble;
		real32 g = capsuleColor->child->next->valuedouble;
		real32 b = capsuleColor->child->next->next->valuedouble;

		if (r >= 0.0f && r <= 1.0f &&
			g >= 0.0f && g <= 1.0f &&
			b >= 0.0f && b <= 1.0f) {
			kmVec3Fill(&config.viewerConfig.capsuleColor, r, g, b);
		}
	}

	// Log Config

	GET_CONFIG_ITEM(viewerFile, "log.files.engine") {
		free(config.logConfig.viewerFile);
		config.logConfig.viewerFile = malloc(
			strlen(viewerFile->valuestring) + 1
		);

		strcpy(config.logConfig.viewerFile, viewerFile->valuestring);
	}

	cJSON_Delete(json);

	return 0;
}

void freeConfig(void) {
	free(config.windowConfig.title);
	free(config.logConfig.viewerFile);
}

void initializeDefaultConfig(void) {
	config.windowConfig.title = malloc(13);
	strcpy(config.windowConfig.title, "Model Viewer");
	config.windowConfig.fullscreen = false;
	config.windowConfig.maximized = false;
	kmVec2Fill(&config.windowConfig.size, 640, 480);
	config.windowConfig.resizable = true;
	config.windowConfig.vsync = true;
	config.windowConfig.numMSAASamples = 4;

	kmVec3Fill(&config.graphicsConfig.backgroundColor, 0.0f, 0.0f, 0.0f);
	config.graphicsConfig.pbr = true;
	config.graphicsConfig.cubemapResolution = 1024;
	config.graphicsConfig.irradianceMapResolution = 64;
	config.graphicsConfig.cubemapDebugMode = false;
	config.graphicsConfig.cubemapDebugMipLevel = -1;

	kmVec3Fill(
		&config.viewerConfig.gridColor,
		117.0f / 255.0f,
		117.0f / 255.0f,
		117.0f / 255.0f
	);

	config.viewerConfig.gridDimensions[0] = 100;
	config.viewerConfig.gridDimensions[1] = 100;
	config.viewerConfig.gridCellSize = 1.0f;

	kmVec3Fill(
		&config.viewerConfig.wireframeColor,
		255.0f / 255.0f,
		235.0f / 255.0f,
		59.0f / 255.0f
	);

	kmVec3Fill(
		&config.viewerConfig.boxColor,
		244.0f / 255.0f,
		67.0f / 255.0f,
		54.0f / 255.0f
	);

	kmVec3Fill(
		&config.viewerConfig.sphereColor,
		76.0f / 255.0f,
		175.0f / 255.0f,
		80.0f / 255.0f
	);

	kmVec3Fill(
		&config.viewerConfig.capsuleColor,
		33.0f / 255.0f,
		150.0f / 255.0f,
		243.0f / 255.0f
	);

	config.logConfig.viewerFile = malloc(11);
	strcpy(config.logConfig.viewerFile, "viewer.log");
}

cJSON* getConfigObject(cJSON *json, const char *key) {
	char *fullKey = malloc(strlen(key) + 1);
	strcpy(fullKey, key);

	const char *name = strtok(fullKey, ".");
	cJSON* object = cJSON_GetObjectItem(json, name);

	while (object) {
		name = strtok(NULL, ".");
		if (!name) {
			break;
		}

		object = cJSON_GetObjectItem(object, name);
	}

	free(fullKey);

	return object;
}

bool cJSONToBool(cJSON *boolObject) {
	return cJSON_IsTrue(boolObject) ? true : false;
}