#include "cubemap_exporter.h"
#include "renderer_utilities.h"
#include "importer.h"
#include "file_utilities.h"
#include "config.h"
#include "log.h"

#define STBI_NO_PIC
#define STBI_NO_PNM
#define STB_IMAGE_IMPLEMENTATION

#include <stb/stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <stb/stb_image_write.h>

#include <sys/stat.h>

#include <dirent.h>

extern Config config;

#define CUBEMAP_MESH_NAME "cubemap"

bool cubemapMeshLoaded;
Mesh cubemapMesh;

#define CUBEMAP_VERTEX_SHADER_FILE "resources/shaders/cubemap.vert"
#define CUBEMAP_CONVERTER_FRAGMENT_SHADER_FILE \
	"resources/shaders/convert_cubemap.frag"

typedef struct cubemap_converter_shader_t {
	GLuint shaderProgram;
	Uniform cubemapTransformUniform;
	Uniform equirectangularCubemapTextureUniform;
} CubemapConverterShader;

#define CUBEMAP_CONVOLUTION_FRAGMENT_SHADER_FILE \
	"resources/shaders/convolute_cubemap.frag"

typedef struct cubemap_convolution_shader_t {
	GLuint shaderProgram;
	Uniform cubemapTransformUniform;
	Uniform cubemapTextureUniform;
} CubemapConvolutionShader;

#define CUBEMAP_PREFILTER_FRAGMENT_SHADER_FILE \
	"resources/shaders/prefilter_cubemap.frag"

typedef struct cubemap_prefilter_shader_t {
	GLuint shaderProgram;
	Uniform cubemapTransformUniform;
	Uniform cubemapTextureUniform;
	Uniform cubemapResolutionUniform;
	Uniform roughnessUniform;
} CubemapPrefilterShader;

internal CubemapConverterShader cubemapConverterShader;
internal CubemapConvolutionShader cubemapConvolutionShader;
internal CubemapPrefilterShader cubemapPrefilterShader;

internal GLuint cubemapFramebuffer;
internal GLuint cubemapRenderbuffer;

internal kmMat4 cubemapTransforms[6];

#define BRDF_LUT_FILE "brdf_lut.hdr"

#define BRDF_LUT_VERTEX_SHADER_FILE "resources/shaders/quad.vert"
#define BRDF_LUT_FRAGMENT_SHADER_FILE "resources/shaders/brdf_lut.frag"

GLuint brdfLUT;

#define NUM_QUAD_VERTEX_ATTRIBUTES 2

typedef struct quad_vertex_t {
	kmVec2 position;
	kmVec2 uv;
} QuadVertex;

Cubemap cubemap = {};

#define CUBEMAP_FOLDER "cubemaps"

const char *cubemapFaceNames[6] = { "+x", "-x", "+y", "-y", "+z", "-z" };

internal int32 loadCubemapMesh(const char *name, Mesh *mesh);
internal void freeCubemapMesh(const char *name, Mesh *mesh);

internal void generateBRDFLUT(void);

internal void initializeCubemapConverterShader(void);
internal void initializeCubemapConvolutionShader(void);
internal void initializeCubemapPrefilterShader(void);

internal int32 exportCubemaps(void);
internal int32 exportCubemap(const char *filePath);

internal int32 uploadCubemap(
	const char *name,
	int32 width,
	int32 height,
	real32 *data,
	GLuint *id
);

internal void convertCubemap(GLuint *equirectangularID, GLuint *cubemapID);
internal void convoluteCubemap(GLuint *cubemapID, GLuint *irradianceID);
internal void prefilterCubemap(GLuint *cubemapID, GLuint *prefilterID);

internal void writeEnvironmentMap(
	const char *name,
	const char *folder,
	GLuint *id
);

internal void writeIrradianceMap(
	const char *name,
	const char *folder,
	GLuint *id
);

internal void writePrefilterMap(
	const char *name,
	const char *folder,
	GLuint *id
);

void initializeCubemapExporter(void) {
	cubemapMeshLoaded = loadCubemapMesh(CUBEMAP_MESH_NAME, &cubemapMesh) != -1;

	initializeCubemapConverterShader();
	initializeCubemapConvolutionShader();
	initializeCubemapPrefilterShader();

	glGenRenderbuffers(1, &cubemapRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, cubemapRenderbuffer);

	glRenderbufferStorage(
		GL_RENDERBUFFER,
		GL_DEPTH_COMPONENT24,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution
	);

	glGenFramebuffers(1, &cubemapFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, cubemapFramebuffer);

	glFramebufferRenderbuffer(
		GL_FRAMEBUFFER,
		GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER,
		cubemapRenderbuffer
	);

	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	kmMat4 cubemapProjection;
	kmMat4PerspectiveProjection(
		&cubemapProjection,
		90.0f,
		1.0f,
		0.1f,
		10.0f
	);

	kmQuaternion cubemapRotations[6];

	// Right Face
	kmQuaternionLookRotation(
		&cubemapRotations[0],
		&KM_VEC3_POS_X,
		&KM_VEC3_NEG_Y
	);

	// Left Face
	kmQuaternionLookRotation(
		&cubemapRotations[1],
		&KM_VEC3_NEG_X,
		&KM_VEC3_NEG_Y
	);

	// Top Face
	kmQuaternionLookRotation(
		&cubemapRotations[2],
		&KM_VEC3_POS_Y,
		&KM_VEC3_POS_Z
	);

	// Bottom Face
	kmQuaternionLookRotation(
		&cubemapRotations[3],
		&KM_VEC3_NEG_Y,
		&KM_VEC3_NEG_Z
	);

	// Front Face
	kmQuaternionLookRotation(
		&cubemapRotations[4],
		&KM_VEC3_POS_Z,
		&KM_VEC3_NEG_Y
	);

	// Back Face
	kmQuaternionLookRotation(
		&cubemapRotations[5],
		&KM_VEC3_NEG_Z,
		&KM_VEC3_NEG_Y
	);

	for (uint8 i = 0; i < 6; i++) {
		kmQuaternionInverse(&cubemapRotations[i], &cubemapRotations[i]);
	}

	kmVec3 cubemapScale;
	kmVec3Fill(&cubemapScale, 1.0f, 1.0f, 1.0f);

	for (uint8 i = 0; i < 6; i++) {
		Transform viewTransform = createTransform(
			&KM_VEC3_ZERO,
			&cubemapRotations[i],
			&cubemapScale
		);

		kmMat4 cubemapView = composeTransform(&viewTransform);
		kmMat4Inverse(&cubemapView, &cubemapView);
		kmMat4Multiply(&cubemapTransforms[i], &cubemapProjection, &cubemapView);
	}

	generateBRDFLUT();
	exportCubemaps();
}

void shutdownCubemapExporter(void) {
	glDeleteProgram(cubemapConverterShader.shaderProgram);
	glDeleteProgram(cubemapConvolutionShader.shaderProgram);
	glDeleteProgram(cubemapPrefilterShader.shaderProgram);

	glDeleteFramebuffers(1, &cubemapFramebuffer);
	glDeleteRenderbuffers(1, &cubemapRenderbuffer);

	glDeleteTextures(1, &brdfLUT);

	freeCubemapMesh(CUBEMAP_MESH_NAME, &cubemapMesh);

	glDeleteTextures(1, &cubemap.cubemapID);
	glDeleteTextures(1, &cubemap.irradianceID);
	glDeleteTextures(1, &cubemap.prefilterID);
}

int32 loadCubemapMesh(const char *name, Mesh *mesh) {
	int32 error = 0;

	LOG("Loading model (%s)...\n", name);

	char *filename = getFullFilePath(name, "mesh", NULL);

	FILE *file = fopen(filename, "rb");

	if (file) {
		uint8 meshBinaryFileVersion;
		fread(&meshBinaryFileVersion, sizeof(uint8), 1, file);

		if (meshBinaryFileVersion < MESH_BINARY_FILE_VERSION) {
			LOG("WARNING: %s out of date\n", filename);
			error = -1;
		}
	} else {
		error = -1;
	}

	if (error != -1) {
		error = loadMesh(mesh, file);
		fclose(file);
	} else {
		LOG("Failed to open %s\n", filename);
		error = -1;
	}

	free(filename);

	if (error != -1) {
		LOG("Successfully loaded model (%s)\n", name);
	} else {
		LOG("Failed to load model (%s)\n", name);
	}

	return error;
}

void freeCubemapMesh(const char *name, Mesh *mesh) {
	LOG("Freeing model (%s)...\n", name);

	freeMesh(mesh);

	LOG("Successfully freed model (%s)\n", name);
}

void generateBRDFLUT(void) {
	QuadVertex quadVertices[4];
	kmVec2Fill(&quadVertices[0].position, -1.0f, 1.0f);
	kmVec2Fill(&quadVertices[0].uv, 0.0f, 1.0f);
	kmVec2Fill(&quadVertices[1].position, -1.0f, -1.0f);
	kmVec2Fill(&quadVertices[1].uv, 0.0f, 0.0f);
	kmVec2Fill(&quadVertices[2].position, 1.0f, 1.0f);
	kmVec2Fill(&quadVertices[2].uv, 1.0f, 1.0f);
	kmVec2Fill(&quadVertices[3].position, 1.0f, -1.0f);
	kmVec2Fill(&quadVertices[3].uv, 1.0f, 0.0f);

	GLuint quadVertexBuffer;
	glGenBuffers(1, &quadVertexBuffer);

	GLuint quadVertexArray;
	glGenVertexArrays(1, &quadVertexArray);

	uint32 bufferIndex = 0;

	glBindBuffer(GL_ARRAY_BUFFER, quadVertexBuffer);
	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(QuadVertex) * 4,
		quadVertices,
		GL_STATIC_DRAW
	);

	glBindVertexArray(quadVertexArray);
	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(QuadVertex),
		(GLvoid*)offsetof(QuadVertex, position)
	);

	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(QuadVertex),
		(GLvoid*)offsetof(QuadVertex, uv)
	);

	glGenTextures(1, &brdfLUT);
	glBindTexture(GL_TEXTURE_2D, brdfLUT);

	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RG16F,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution,
		0,
		GL_RG,
		GL_FLOAT,
		NULL
	);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	GLuint shaderProgram;
	createShaderProgram(
		BRDF_LUT_VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		BRDF_LUT_FRAGMENT_SHADER_FILE,
		NULL,
		&shaderProgram
	);

	glViewport(
		0,
		0,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution
	);

	glBindRenderbuffer(GL_RENDERBUFFER, cubemapRenderbuffer);
	glRenderbufferStorage(
		GL_RENDERBUFFER,
		GL_DEPTH_COMPONENT24,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution
	);

	glBindFramebuffer(GL_FRAMEBUFFER, cubemapFramebuffer);
	glFramebufferTexture2D(
		GL_FRAMEBUFFER,
		GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D,
		brdfLUT,
		0
	);

	glUseProgram(shaderProgram);

	glBindVertexArray(quadVertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, quadVertexBuffer);

	for (uint8 i = 0; i < NUM_QUAD_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	logGLError(false, "Failed to generate BRDF LUT");

	for (uint8 i = 0; i < NUM_QUAD_VERTEX_ATTRIBUTES; i++) {
		glDisableVertexAttribArray(i);
	}

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glDeleteProgram(shaderProgram);

	glBindVertexArray(quadVertexArray);
	glDeleteBuffers(1, &quadVertexBuffer);
	glBindVertexArray(0);

	glDeleteVertexArrays(1, &quadVertexArray);

	real32 *data = malloc(
		3 * sizeof(real32) *
		config.graphicsConfig.cubemapResolution *
		config.graphicsConfig.cubemapResolution
	);

	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, data);
	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_write_hdr(
		BRDF_LUT_FILE,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution,
		3,
		data
	);

	free(data);
}

void initializeCubemapConverterShader(void) {
	createShaderProgram(
		CUBEMAP_VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		CUBEMAP_CONVERTER_FRAGMENT_SHADER_FILE,
		NULL,
		&cubemapConverterShader.shaderProgram
	);

	getUniform(
		cubemapConverterShader.shaderProgram,
		"cubemapTransform",
		UNIFORM_MAT4,
		&cubemapConverterShader.cubemapTransformUniform
	);

	getUniform(
		cubemapConverterShader.shaderProgram,
		"equirectangularCubemapTexture",
		UNIFORM_TEXTURE_2D,
		&cubemapConverterShader.equirectangularCubemapTextureUniform
	);
}

void initializeCubemapConvolutionShader(void) {
	createShaderProgram(
		CUBEMAP_VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		CUBEMAP_CONVOLUTION_FRAGMENT_SHADER_FILE,
		NULL,
		&cubemapConvolutionShader.shaderProgram
	);

	getUniform(
		cubemapConvolutionShader.shaderProgram,
		"cubemapTransform",
		UNIFORM_MAT4,
		&cubemapConvolutionShader.cubemapTransformUniform
	);

	getUniform(
		cubemapConvolutionShader.shaderProgram,
		"cubemapTexture",
		UNIFORM_TEXTURE_CUBE_MAP,
		&cubemapConvolutionShader.cubemapTextureUniform
	);
}

void initializeCubemapPrefilterShader(void) {
	createShaderProgram(
		CUBEMAP_VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		CUBEMAP_PREFILTER_FRAGMENT_SHADER_FILE,
		NULL,
		&cubemapPrefilterShader.shaderProgram
	);

	getUniform(
		cubemapPrefilterShader.shaderProgram,
		"cubemapTransform",
		UNIFORM_MAT4,
		&cubemapPrefilterShader.cubemapTransformUniform
	);

	getUniform(
		cubemapPrefilterShader.shaderProgram,
		"cubemapTexture",
		UNIFORM_TEXTURE_CUBE_MAP,
		&cubemapPrefilterShader.cubemapTextureUniform
	);

	getUniform(
		cubemapPrefilterShader.shaderProgram,
		"cubemapResolution",
		UNIFORM_FLOAT,
		&cubemapPrefilterShader.cubemapResolutionUniform
	);

	getUniform(
		cubemapPrefilterShader.shaderProgram,
		"roughness",
		UNIFORM_FLOAT,
		&cubemapPrefilterShader.roughnessUniform
	);
}

int32 exportCubemaps(void) {
	DIR *dir = opendir(CUBEMAP_FOLDER);
	if (dir) {
		struct dirent *dirEntry = readdir(dir);
		while (dirEntry) {
			if (strcmp(dirEntry->d_name, ".") &&
				strcmp(dirEntry->d_name, "..")) {
				char *folderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					CUBEMAP_FOLDER
				);

				struct stat info;
				stat(folderPath, &info);

				if (S_ISREG(info.st_mode)) {
					char *extension = getExtension(dirEntry->d_name);
					if (extension && !strcmp(extension, "hdr")) {
						exportCubemap(folderPath);
					}

					free(extension);
				}

				free(folderPath);
			}

			dirEntry = readdir(dir);
		}

		closedir(dir);
	} else {
		LOG("Failed to open %s/\n", CUBEMAP_FOLDER);
		return -1;
	}

	return 0;
}

int32 exportCubemap(const char *filePath) {
	int32 error = 0;

	char *filename = removeExtension(filePath);
	char *cubemapName = strrchr(filename, '/') + 1;

	LOG("Exporting cubemap (%s)...\n", cubemapName);
	LOG("Loading cubemap (%s)...\n", cubemapName);

	int32 width, height, numComponents;
	real32 *data = stbi_loadf(
		filePath,
		&width,
		&height,
		&numComponents,
		3
	);

	if (!data) {
		LOG("Failed to load cubemap\n");
		error = -1;
	}

	if (error != -1) {
		stbi__vertical_flip(
			data,
			width,
			height,
			3 * sizeof(real32)
		);

		GLuint equirectangularID;
		error = uploadCubemap(
			cubemapName,
			width,
			height,
			data,
			&equirectangularID
		);

		LOG("Successfully loaded cubemap (%s)\n", cubemapName);

		GLuint cubemapID = 0;
		GLuint irradianceID = 0;
		GLuint prefilterID = 0;

		if (error != -1) {
			LOG("Converting cubemap (%s)...\n", cubemapName);

			convertCubemap(&equirectangularID, &cubemapID);
			convoluteCubemap(&cubemapID, &irradianceID);
			prefilterCubemap(&cubemapID, &prefilterID);

			deleteFolder(filename, false, &logFunction);
			MKDIR(filename);

			writeEnvironmentMap(cubemapName, filename, &cubemapID);
			writeIrradianceMap(cubemapName, filename, &irradianceID);
			writePrefilterMap(cubemapName, filename, &prefilterID);

			LOG("Successfully converted cubemap (%s)\n", cubemapName);
		}

		if (strlen(cubemap.name.string) == 0) {
			cubemap.name = stringToUUID(cubemapName);
			cubemap.cubemapID = cubemapID;
			cubemap.irradianceID = irradianceID;
			cubemap.prefilterID = prefilterID;
		} else {
			glDeleteTextures(1, &cubemapID);
			glDeleteTextures(1, &irradianceID);
			glDeleteTextures(1, &prefilterID);
		}

		LOG("Successfully exported cubemap (%s)\n", cubemapName);
	} else {
		LOG("Failed to export cubemap (%s)\n", cubemapName);
	}

	free(filename);

	return error;
}

int32 uploadCubemap(
	const char *name,
	int32 width,
	int32 height,
	real32 *data,
	GLuint *id
) {
	int32 error = 0;

	LOG("Transferring cubemap (%s) onto GPU...\n", name);

	glGenTextures(1, id);
	glBindTexture(GL_TEXTURE_2D, *id);

	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGB16F,
		width,
		height,
		0,
		GL_RGB,
		GL_FLOAT,
		data
	);

	free(data);

	error = logGLError(false, "Failed to transfer cubemap onto GPU");

	if (error != -1) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		LOG("Successfully transferred cubemap (%s) onto GPU\n", name);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	return error;
}

void convertCubemap(GLuint *equirectangularID, GLuint *cubemapID) {
	glGenTextures(1, cubemapID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemapID);

	for (uint8 i = 0; i < 6; i++) {
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0,
			GL_RGB16F,
			config.graphicsConfig.cubemapResolution,
			config.graphicsConfig.cubemapResolution,
			0,
			GL_RGB,
			GL_FLOAT,
			NULL
		);
	}

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MAG_FILTER,
		GL_LINEAR
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_S,
		GL_CLAMP_TO_EDGE
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_T,
		GL_CLAMP_TO_EDGE
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_R,
		GL_CLAMP_TO_EDGE
	);

	glViewport(
		0,
		0,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution
	);

	glBindRenderbuffer(GL_RENDERBUFFER, cubemapRenderbuffer);
	glRenderbufferStorage(
		GL_RENDERBUFFER,
		GL_DEPTH_COMPONENT24,
		config.graphicsConfig.cubemapResolution,
		config.graphicsConfig.cubemapResolution
	);

	glBindFramebuffer(GL_FRAMEBUFFER, cubemapFramebuffer);
	glUseProgram(cubemapConverterShader.shaderProgram);

	uint32 textureIndex = 0;
	setUniform(
		cubemapConverterShader.equirectangularCubemapTextureUniform,
		1,
		&textureIndex
	);

	glBindVertexArray(cubemapMesh.vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubemapMesh.indexBuffer);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, *equirectangularID);

	glFrontFace(GL_CW);

	for (uint8 i = 0; i < 6; i++) {
		glFramebufferTexture2D(
			GL_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			*cubemapID,
			0
		);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		setUniform(
			cubemapConverterShader.cubemapTransformUniform,
			1,
			&cubemapTransforms[i]
		);

		glDrawElements(
			GL_TRIANGLES,
			cubemapMesh.numIndices,
			GL_UNSIGNED_INT,
			NULL
		);

		logGLError(false, "Failed to convert cubemap");
	}

	glFrontFace(GL_CCW);

	glBindTexture(GL_TEXTURE_2D, 0);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glDisableVertexAttribArray(i);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glDeleteTextures(1, equirectangularID);
	*equirectangularID = 0;

	glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemapID);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void convoluteCubemap(GLuint *cubemapID, GLuint *irradianceID) {
	glGenTextures(1, irradianceID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *irradianceID);

	for (uint8 i = 0; i < 6; i++) {
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0,
			GL_RGB16F,
			config.graphicsConfig.irradianceMapResolution,
			config.graphicsConfig.irradianceMapResolution,
			0,
			GL_RGB,
			GL_FLOAT,
			NULL
		);
	}

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MIN_FILTER,
		GL_LINEAR
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MAG_FILTER,
		GL_LINEAR
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_S,
		GL_CLAMP_TO_EDGE
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_T,
		GL_CLAMP_TO_EDGE
	);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_WRAP_R,
		GL_CLAMP_TO_EDGE
	);

	glViewport(
		0,
		0,
		config.graphicsConfig.irradianceMapResolution,
		config.graphicsConfig.irradianceMapResolution
	);

	glBindRenderbuffer(GL_RENDERBUFFER, cubemapRenderbuffer);
	glRenderbufferStorage(
		GL_RENDERBUFFER,
		GL_DEPTH_COMPONENT24,
		config.graphicsConfig.irradianceMapResolution,
		config.graphicsConfig.irradianceMapResolution
	);

	glBindFramebuffer(GL_FRAMEBUFFER, cubemapFramebuffer);
	glUseProgram(cubemapConvolutionShader.shaderProgram);

	uint32 textureIndex = 0;
	setUniform(
		cubemapConvolutionShader.cubemapTextureUniform,
		1,
		&textureIndex
	);

	glBindVertexArray(cubemapMesh.vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubemapMesh.indexBuffer);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemapID);

	glFrontFace(GL_CW);

	for (uint8 i = 0; i < 6; i++) {
		glFramebufferTexture2D(
			GL_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			*irradianceID,
			0
		);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		setUniform(
			cubemapConvolutionShader.cubemapTransformUniform,
			1,
			&cubemapTransforms[i]
		);

		glDrawElements(
			GL_TRIANGLES,
			cubemapMesh.numIndices,
			GL_UNSIGNED_INT,
			NULL
		);

		logGLError(false, "Failed to convolute cubemap");
	}

	glFrontFace(GL_CCW);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glDisableVertexAttribArray(i);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void prefilterCubemap(GLuint *cubemapID, GLuint *prefilterID) {
	glGenTextures(1, prefilterID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *prefilterID);

	for (uint8 i = 0; i < 6; i++) {
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0,
			GL_RGB16F,
			config.graphicsConfig.prefilterMapResolution,
			config.graphicsConfig.prefilterMapResolution,
			0,
			GL_RGB,
			GL_FLOAT,
			NULL
		);
	}

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR
	);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glBindFramebuffer(GL_FRAMEBUFFER, cubemapFramebuffer);
	glUseProgram(cubemapPrefilterShader.shaderProgram);

	uint32 textureIndex = 0;
	setUniform(
		cubemapPrefilterShader.cubemapTextureUniform,
		1,
		&textureIndex
	);

	setUniform(
		cubemapPrefilterShader.cubemapResolutionUniform,
		1,
		&config.graphicsConfig.cubemapResolution
	);

	glBindVertexArray(cubemapMesh.vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubemapMesh.indexBuffer);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemapID);

	glFrontFace(GL_CW);

	const uint32 maxNumMipLevels = 5;
	for (uint8 mipLevel = 0; mipLevel < maxNumMipLevels; mipLevel++) {
		uint32 mipLevelSize =
			config.graphicsConfig.prefilterMapResolution * powf(0.5f, mipLevel);

		glViewport(0, 0, mipLevelSize, mipLevelSize);
		glBindRenderbuffer(GL_RENDERBUFFER, cubemapRenderbuffer);
		glRenderbufferStorage(
			GL_RENDERBUFFER,
			GL_DEPTH_COMPONENT24,
			mipLevelSize,
			mipLevelSize
		);

		real32 roughness = (real32)mipLevel / (real32)(maxNumMipLevels - 1);
		setUniform(cubemapPrefilterShader.roughnessUniform, 1, &roughness);

		for (uint8 i = 0; i < 6; i++) {
			glFramebufferTexture2D(
				GL_FRAMEBUFFER,
				GL_COLOR_ATTACHMENT0,
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				*prefilterID,
				mipLevel
			);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			setUniform(
				cubemapPrefilterShader.cubemapTransformUniform,
				1,
				&cubemapTransforms[i]
			);

			glDrawElements(
				GL_TRIANGLES,
				cubemapMesh.numIndices,
				GL_UNSIGNED_INT,
				NULL
			);

			logGLError(false, "Failed to prefilter cubemap");
		}
	}

	glFrontFace(GL_CCW);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	for (uint8 i = 0; i < NUM_VERTEX_ATTRIBUTES; i++) {
		glDisableVertexAttribArray(i);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemapID);
	glTexParameteri(
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_MIN_FILTER,
		GL_LINEAR
	);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void writeEnvironmentMap(
	const char *name,
	const char *folder,
	GLuint *id
) {
	glBindTexture(GL_TEXTURE_CUBE_MAP, *id);

	real32 *data[6];
	for (uint8 i = 0; i < 6; i++) {
		data[i] = malloc(
			3 *
			sizeof(real32) *
			config.graphicsConfig.cubemapResolution *
			config.graphicsConfig.cubemapResolution
		);

		glGetTexImage(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0,
			GL_RGB,
			GL_FLOAT,
			data[i]
		);
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	char *fullFolder = getFullFilePath(
		"environment",
		NULL,
		folder
	);

	MKDIR(fullFolder);

	char *filePath = getFullFilePath(
		name,
		NULL,
		fullFolder
	);

	free(fullFolder);

	char *filename = concatenateStrings(filePath , "_", "environment");
	free(filePath);

	char *filenames[6];
	for (uint8 i = 0; i < 6; i++) {
		filenames[i] = concatenateStrings(
			filename,
			cubemapFaceNames[i],
			".hdr"
		);
	}

	free(filename);

	for (uint8 i = 0; i < 6; i++) {
		stbi_write_hdr(
			filenames[i],
			config.graphicsConfig.cubemapResolution,
			config.graphicsConfig.cubemapResolution,
			3,
			data[i]
		);

		free(filenames[i]);
		free(data[i]);
	}
}

void writeIrradianceMap(
	const char *name,
	const char *folder,
	GLuint *id
) {
	glBindTexture(GL_TEXTURE_CUBE_MAP, *id);

	real32 *data[6];
	for (uint8 i = 0; i < 6; i++) {
		data[i] = malloc(
			3 *
			sizeof(real32) *
			config.graphicsConfig.irradianceMapResolution *
			config.graphicsConfig.irradianceMapResolution
		);

		glGetTexImage(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
			0,
			GL_RGB,
			GL_FLOAT,
			data[i]
		);
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	char *fullFolder = getFullFilePath(
		"irradiance",
		NULL,
		folder
	);

	MKDIR(fullFolder);

	char *filePath = getFullFilePath(
		name,
		NULL,
		fullFolder
	);

	free(fullFolder);

	char *filename = concatenateStrings(filePath , "_", "irradiance");
	free(filePath);

	char *filenames[6];
	for (uint8 i = 0; i < 6; i++) {
		filenames[i] = concatenateStrings(
			filename,
			cubemapFaceNames[i],
			".hdr"
		);
	}

	free(filename);

	for (uint8 i = 0; i < 6; i++) {
		stbi_write_hdr(
			filenames[i],
			config.graphicsConfig.irradianceMapResolution,
			config.graphicsConfig.irradianceMapResolution,
			3,
			data[i]
		);

		free(filenames[i]);
		free(data[i]);
	}
}

void writePrefilterMap(
	const char *name,
	const char *folder,
	GLuint *id
) {
	char *prefilterFolder = getFullFilePath(
		"prefilter",
		NULL,
		folder
	);

	MKDIR(prefilterFolder);

	glBindTexture(GL_TEXTURE_CUBE_MAP, *id);

	const uint32 maxNumMipLevels = 5;
	for (uint8 mipLevel = 0; mipLevel < maxNumMipLevels; mipLevel++) {
		uint32 resolution =
			config.graphicsConfig.prefilterMapResolution * powf(0.5f, mipLevel);

		real32 *data[6];
		for (uint8 i = 0; i < 6; i++) {
			data[i] = malloc(3 * sizeof(real32) * resolution * resolution);
			glGetTexImage(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				mipLevel,
				GL_RGB,
				GL_FLOAT,
				data[i]
			);
		}

		char mipLevelName[12];
		sprintf(mipLevelName, "mip_level_%d", mipLevel);

		char *fullFolder = getFullFilePath(
			mipLevelName,
			NULL,
			prefilterFolder
		);

		MKDIR(fullFolder);

		char *filePath = getFullFilePath(
			name,
			NULL,
			fullFolder
		);

		free(fullFolder);

		char *filename = concatenateStrings(filePath , "_", "prefilter");
		free(filePath);

		char extension[7];
		sprintf(extension, "_%d.hdr", mipLevel);

		char *filenames[6];
		for (uint8 i = 0; i < 6; i++) {
			filenames[i] = concatenateStrings(
				filename,
				cubemapFaceNames[i],
				extension
			);
		}

		free(filename);

		for (uint8 i = 0; i < 6; i++) {
			stbi_write_hdr(
				filenames[i],
				resolution,
				resolution,
				3,
				data[i]
			);

			free(filenames[i]);
			free(data[i]);
		}
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	free(prefilterFolder);
}