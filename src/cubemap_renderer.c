#include "cubemap_renderer.h"
#include "renderer_utilities.h"
#include "cubemap_exporter.h"
#include "file_utilities.h"
#include "importer.h"
#include "config.h"
#include "log.h"

#include <string.h>

#define VERTEX_SHADER_FILE "resources/shaders/render_cubemap.vert"
#define FRAGMENT_SHADER_FILE "resources/shaders/render_cubemap.frag"

internal GLuint shaderProgram;

internal Uniform modelUniform;
internal Uniform viewUniform;
internal Uniform projectionUniform;

internal Uniform cubemapTextureUniform;
internal Uniform cubemapMipLevelUniform;

extern Config config;

extern bool cubemapMeshLoaded;
extern Mesh cubemapMesh;

extern Cubemap cubemap;

int32 initializeCubemapRenderer(void) {
	LOG("Initializing cubemap renderer...\n");

	if (createShaderProgram(
		VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		FRAGMENT_SHADER_FILE,
		NULL,
		&shaderProgram) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "model", UNIFORM_MAT4, &modelUniform) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "view", UNIFORM_MAT4, &viewUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"projection",
		UNIFORM_MAT4,
		&projectionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"cubemapTexture",
		UNIFORM_TEXTURE_CUBE_MAP,
		&cubemapTextureUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"cubemapMipLevel",
		UNIFORM_FLOAT,
		&cubemapMipLevelUniform) == -1) {
		return -1;
	}

	LOG("Successfully initialized cubemap renderer\n");

	return 0;
}

int32 drawCubemap(Camera *camera) {
	if (!cubemapMeshLoaded || strlen(cubemap.name.string) == 0) {
		return 0;
	}

	glUseProgram(shaderProgram);

	Camera cubemapCamera = *camera;
	kmVec3Zero(&cubemapCamera.position);
	kmVec3Zero(&cubemapCamera.translation);

	if (setCameraUniforms(
		cubemapCamera,
		&viewUniform,
		&projectionUniform) == 1) {
		return -1;
	}

	kmMat4 worldMatrix;
	kmMat4Scaling(&worldMatrix, 2.0f, 2.0f, 2.0f);

	if (setUniform(modelUniform, 1, &worldMatrix) == -1) {
		return -1;
	}

	GLint textureIndex = 0;
	if (setUniform(cubemapTextureUniform, 1, &textureIndex) == -1) {
		return -1;
	}

	if (setUniform(
		cubemapMipLevelUniform,
		1,
		&config.graphicsConfig.cubemapDebugMipLevel) == -1) {
		return -1;
	}

	glBindVertexArray(cubemapMesh.vertexArray);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubemapMesh.indexBuffer);

	for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
		glEnableVertexAttribArray(j);
	}

	GLuint cubemapTexture = cubemap.cubemapID;
	if (config.graphicsConfig.cubemapDebugMode) {
		if (config.graphicsConfig.cubemapDebugMipLevel >= 0.0f) {
			cubemapTexture = cubemap.prefilterID;
		} else {
			cubemapTexture = cubemap.irradianceID;
		}
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);

	glDepthMask(GL_FALSE);
	glFrontFace(GL_CW);

	glDrawElements(
		GL_TRIANGLES,
		cubemapMesh.numIndices,
		GL_UNSIGNED_INT,
		NULL
	);

	logGLError(false, "Failed to draw cubemap");

	glDepthMask(GL_TRUE);
	glFrontFace(GL_CCW);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
		glDisableVertexAttribArray(j);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	return 0;
}

void shutdownCubemapRenderer(void) {
	LOG("Shutting down cubemap renderer...\n");

	glDeleteProgram(shaderProgram);

	LOG("Successfully shut down cubemap renderer\n");
}