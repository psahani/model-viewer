#include "debug_renderer.h"
#include "renderer_utilities.h"
#include "log.h"

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stddef.h>

#define NUM_DEBUG_VERTEX_ATTRIBUTES 2

typedef struct debug_vertex_t {
	kmVec3 position;
	kmVec3 color;
} DebugVertex;

internal DebugVertex vertices[MAX_DEBUG_VERTEX_COUNT];
internal uint32 numVertices;

internal GLuint vertexBuffer;
internal GLuint vertexArray;

#define VERTEX_SHADER_FILE "resources/shaders/line.vert"
#define FRAGMENT_SHADER_FILE "resources/shaders/line.frag"

internal GLuint shaderProgram;

internal Uniform viewUniform;
internal Uniform projectionUniform;

internal void clearVertices(void);
internal void addVertex(DebugVertex vertex);

int32 initializeDebugRenderer(void) {
	LOG("Initializing debug renderer...\n");

	clearVertices();

	glGenBuffers(1, &vertexBuffer);
	glGenVertexArrays(1, &vertexArray);

	uint32 bufferIndex = 0;

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(DebugVertex) * MAX_DEBUG_VERTEX_COUNT,
		NULL,
		GL_DYNAMIC_DRAW
	);

	glBindVertexArray(vertexArray);
	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(DebugVertex),
		(GLvoid*)offsetof(DebugVertex, position)
	);

	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(DebugVertex),
		(GLvoid*)offsetof(DebugVertex, color)
	);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	if (createShaderProgram(
		VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		FRAGMENT_SHADER_FILE,
		NULL,
		&shaderProgram) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "view", UNIFORM_MAT4, &viewUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"projection",
		UNIFORM_MAT4,
		&projectionUniform) == -1) {
		return -1;
	}

	LOG("Successfully initialized debug renderer\n");

	return 0;
}

void drawLine(
	const kmVec3 *positionA,
	const kmVec3 *positionB,
	const kmVec3 *color
) {
	DebugVertex vertex;
	memset(&vertex, 0, sizeof(DebugVertex));

	kmVec3Assign(&vertex.position, positionA);
	kmVec3Assign(&vertex.color, color);
	addVertex(vertex);

	kmVec3Assign(&vertex.position, positionB);
	addVertex(vertex);
}

void drawTransform(Transform transform, real32 size) {
	kmVec3 origin = KM_VEC3_ZERO;
	kmVec3 xAxis = KM_VEC3_POS_X;
	kmVec3 yAxis = KM_VEC3_POS_Y;
	kmVec3 zAxis = KM_VEC3_POS_Z;

	kmVec3Scale(&xAxis, &xAxis, size);
	kmVec3Scale(&yAxis, &yAxis, size);
	kmVec3Scale(&zAxis, &zAxis, size);

	kmVec3Fill(&transform.scale, 1.0f, 1.0f, 1.0f);

	origin = transformVertex(&origin, &transform);
	xAxis = transformVertex(&xAxis, &transform);
	yAxis = transformVertex(&yAxis, &transform);
	zAxis = transformVertex(&zAxis, &transform);

	kmVec3 red = KM_VEC3_POS_X;
	kmVec3 green = KM_VEC3_POS_Y;
	kmVec3 blue = KM_VEC3_POS_Z;

	drawLine(&origin, &xAxis, &red);
	drawLine(&origin, &yAxis, &green);
	drawLine(&origin, &zAxis, &blue);
}

void drawGrid(uint32 rows, uint32 columns, real32 cellSize, kmVec3 *color) {
	real32 width = columns * cellSize;
	real32 halfWidth = width / 2.0f;

	real32 depth = rows * cellSize;
	real32 halfDepth = depth / 2.0f;

	for (real32 x = -halfWidth; x <= halfWidth; x += cellSize) {
		kmVec3 positionA;
		kmVec3Fill(&positionA, x, 0.0f, halfDepth);

		kmVec3 positionB;
		kmVec3Fill(&positionB, x, 0.0f, -halfDepth);

		drawLine(&positionA, &positionB, color);
	}

	for (real32 z = -halfDepth; z <= halfDepth; z += cellSize) {
		kmVec3 positionA;
		kmVec3Fill(&positionA, -halfWidth, 0.0f, z);

		kmVec3 positionB;
		kmVec3Fill(&positionB, halfWidth, 0.0f, z);

		drawLine(&positionA, &positionB, color);
	}
}

int32 drawDebugLines(Camera *camera) {
	if (numVertices == 0) {
		return 0;
	}

	glUseProgram(shaderProgram);

	if (setCameraUniforms(*camera, &viewUniform, &projectionUniform) == 1) {
		return -1;
	}

	glBindVertexArray(vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

	void *buffer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	memcpy(
		buffer,
		vertices,
		sizeof(DebugVertex) * MAX_DEBUG_VERTEX_COUNT
	);
	glUnmapBuffer(GL_ARRAY_BUFFER);

	for (uint8 i = 0; i < NUM_DEBUG_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glDrawArrays(GL_LINES, 0, numVertices);

	if (logGLError(false, "Failed to draw debug line") == -1) {
		return -1;
	}

	for (uint8 j = 0; j < NUM_DEBUG_VERTEX_ATTRIBUTES; j++) {
		glDisableVertexAttribArray(j);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	clearVertices();

	return 0;
}

void shutdownDebugRenderer(void) {
	LOG("Shutting down debug renderer...\n");

	glBindVertexArray(vertexArray);
	glDeleteBuffers(1, &vertexBuffer);
	glBindVertexArray(0);

	glDeleteVertexArrays(1, &vertexArray);

	glDeleteProgram(shaderProgram);

	LOG("Successfully shut down debug renderer\n");
}

void clearVertices(void) {
	memset(vertices, 0, sizeof(DebugVertex) * MAX_DEBUG_VERTEX_COUNT);
	numVertices = 0;
}

void addVertex(DebugVertex vertex) {
	if (numVertices < MAX_DEBUG_VERTEX_COUNT) {
		vertices[numVertices++] = vertex;
	}
}