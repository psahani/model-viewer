#include "gui.h"
#include "log.h"
#include "renderer_utilities.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING

#define NK_IMPLEMENTATION
#include <nuklear/nuklear.h>

#include <kazmath/vec4.h>

#include <stddef.h>
#include <string.h>

#define NUM_GUI_VERTEX_ATTRIBUTES 3

typedef struct gui_vertex_t {
	kmVec2 position;
	kmVec2 uv;
	kmVec4 color;
} GUIVertex;

internal GLuint vertexBuffer;
internal GLuint vertexArray;
internal GLuint indexBuffer;

#define VERTEX_SHADER_FILE "resources/shaders/gui.vert"
#define FRAGMENT_SHADER_FILE "resources/shaders/gui.frag"

internal GLuint shaderProgram;

internal Uniform projectionUniform;

internal Uniform fontUniform;

internal struct nk_context ctx;
internal struct nk_font_atlas atlas;
internal GLuint textureID;
internal struct nk_draw_null_texture null;
internal struct nk_buffer cmds;
struct nk_colorf color;

int32 initializeGUI(void) {
	memset(&color, 0, sizeof(struct nk_colorf));

	glGenBuffers(1, &vertexBuffer);
	glGenVertexArrays(1, &vertexArray);

	uint32 bufferIndex = 0;

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBindVertexArray(vertexArray);

	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(GUIVertex),
		(GLvoid*)offsetof(GUIVertex, position)
	);

	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(GUIVertex),
		(GLvoid*)offsetof(GUIVertex, uv)
	);

	glVertexAttribPointer(
		bufferIndex++,
		4,
		GL_FLOAT,
		GL_FALSE,
		sizeof(GUIVertex),
		(GLvoid*)offsetof(GUIVertex, color)
	);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenBuffers(1, &indexBuffer);

	if (createShaderProgram(
		VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		FRAGMENT_SHADER_FILE,
		NULL,
		&shaderProgram) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"projection",
		UNIFORM_MAT4,
		&projectionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"font",
		UNIFORM_TEXTURE_2D,
		&fontUniform) == -1) {
		return -1;
	}

	int32 error = nk_init_default(&ctx, NULL) ? 0 : -1;

	if (error != -1) {
		nk_buffer_init_default(&cmds);

		nk_font_atlas_init_default(&atlas);
		nk_font_atlas_begin(&atlas);

		struct nk_font *font = nk_font_atlas_add_from_file(
			&atlas,
			"resources/fonts/roboto.ttf",
			18,
			NULL
		);

		int32 width, height;
		const void *image = nk_font_atlas_bake(
			&atlas,
			&width,
			&height,
			NK_FONT_ATLAS_RGBA32
		);

		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RGBA,
			width,
			height,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			image
		);

		error = logGLError(false, "Failed to transfer font onto GPU");

		glBindTexture(GL_TEXTURE_2D, 0);

		if (error != -1) {
			nk_font_atlas_end(&atlas, nk_handle_id(textureID), &null);
			nk_style_set_font(&ctx, &font->handle);
		}
	}

	return error;
}

void updateGUI(GLFWwindow* window) {
	nk_input_begin(&ctx);

	real64 x, y;
	glfwGetCursorPos(window, &x, &y);
	nk_input_motion(&ctx, x, y);

	nk_input_button(
		&ctx,
		NK_BUTTON_LEFT,
		x,
		y,
		glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS
	);

    nk_input_button(
		&ctx,
		NK_BUTTON_MIDDLE,
		x,
		y,
		glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS
	);

	nk_input_button(
		&ctx,
		NK_BUTTON_RIGHT,
		x,
		y,
		glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS
	);

	nk_input_end(&ctx);
}

int32 drawGUI(kmVec2 scale, int32 width, int32 height) {
	if (nk_begin(
		&ctx,
		"Test",
		nk_rect(50, 50, 230, 250),
		NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
		NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {
		nk_layout_row_static(&ctx, 30, 80, 1);
		if (nk_button_label(&ctx, "button")) {
			LOG("Button pressed\n");
		}

		static bool option = true;

		nk_layout_row_dynamic(&ctx, 30, 2);
		if (nk_option_label(&ctx, "Option 1", option == true)) {
			option = true;
		}

		if (nk_option_label(&ctx, "Option 2", option == false)) {
			option = false;
		}

		static int32 speed = 20;

		nk_layout_row_dynamic(&ctx, 25, 1);
		nk_property_int(&ctx, "Speed:", 0, &speed, 100, 10, 1);

		nk_layout_row_dynamic(&ctx, 20, 1);
		nk_label(&ctx, "Color:", NK_TEXT_LEFT);
		nk_layout_row_dynamic(&ctx, 25, 1);

		if (nk_combo_begin_color(
			&ctx,
			nk_rgb_cf(color),
			nk_vec2(nk_widget_width(&ctx), 400))
		) {
			nk_layout_row_dynamic(&ctx, 120, 1);
			color = nk_color_picker(&ctx, color, NK_RGBA);
			nk_layout_row_dynamic(&ctx, 25, 1);
			color.r = nk_propertyf(&ctx, "#R:", 0, color.r, 1.0f, 0.01f,0.005f);
			color.g = nk_propertyf(&ctx, "#G:", 0, color.g, 1.0f, 0.01f,0.005f);
			color.b = nk_propertyf(&ctx, "#B:", 0, color.b, 1.0f, 0.01f,0.005f);
			color.a = nk_propertyf(&ctx, "#A:", 0, color.a, 1.0f, 0.01f,0.005f);
			nk_combo_end(&ctx);
		}
	}

	nk_end(&ctx);

	kmMat4 projectionMatrix;
	kmMat4OrthographicProjection(
		&projectionMatrix,
		0.0f,
		width,
		height,
		0.0f,
		0.0f,
		2.0f
	);

	glUseProgram(shaderProgram);

	if (setUniform(projectionUniform, 1, &projectionMatrix) == -1) {
		return -1;
	}

	glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

	glBindVertexArray(vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(GUIVertex) * MAX_GUI_VERTEX_COUNT,
		NULL,
		GL_STREAM_DRAW
	);

	void *vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		sizeof(uint16) * MAX_GUI_INDEX_COUNT,
		NULL,
		GL_STREAM_DRAW
	);

	void *indices = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

	static const struct nk_draw_vertex_layout_element vertex_layout[] = {
		{
			NK_VERTEX_POSITION,
			NK_FORMAT_FLOAT,
			NK_OFFSETOF(GUIVertex, position)
		},

		{
			NK_VERTEX_TEXCOORD,
			NK_FORMAT_FLOAT,
			NK_OFFSETOF(GUIVertex, uv)
		},

		{
			NK_VERTEX_COLOR,
			NK_FORMAT_R32G32B32A32_FLOAT,
			NK_OFFSETOF(GUIVertex, color)
		},

		{ NK_VERTEX_LAYOUT_END }
	};

	struct nk_convert_config config = {};
	config.shape_AA = NK_ANTI_ALIASING_ON;
	config.line_AA = NK_ANTI_ALIASING_ON;
	config.vertex_layout = vertex_layout;
	config.vertex_size = sizeof(GUIVertex);
	config.vertex_alignment = NK_ALIGNOF(GUIVertex);
	config.circle_segment_count = 22;
	config.curve_segment_count = 22;
	config.arc_segment_count = 22;
	config.global_alpha = 1.0f;
	config.null = null;

	struct nk_buffer vertexBufferData, indexBufferData;
	nk_buffer_init_fixed(
		&vertexBufferData,
		vertices,
		sizeof(GUIVertex) * MAX_GUI_VERTEX_COUNT
	);

	nk_buffer_init_fixed(
		&indexBufferData,
		indices,
		sizeof(uint16) * MAX_GUI_INDEX_COUNT
	);

	nk_convert(&ctx, &cmds, &vertexBufferData, &indexBufferData, &config);

	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	glUnmapBuffer(GL_ARRAY_BUFFER);

	const struct nk_draw_command *cmd;
	const nk_draw_index *offset = NULL;

	for (uint8 i = 0; i < NUM_GUI_VERTEX_ATTRIBUTES; i++) {
		glEnableVertexAttribArray(i);
	}

	glActiveTexture(GL_TEXTURE0);

	GLint textureIndex = 0;
	if (setUniform(fontUniform, 1, &textureIndex) == -1) {
		return -1;
	}

	nk_draw_foreach(cmd, &ctx, &cmds) {
		if (!cmd->elem_count) {
			continue;
		}

		glBindTexture(GL_TEXTURE_2D, cmd->texture.id);

		glScissor(
			cmd->clip_rect.x * scale.x,
			(height - (cmd->clip_rect.y + cmd->clip_rect.h)) * scale.y,
			cmd->clip_rect.w * scale.x,
			cmd->clip_rect.h * scale.y
		);

		glDrawElements(
			GL_TRIANGLES,
			cmd->elem_count,
			GL_UNSIGNED_SHORT,
			offset
		);

		offset += cmd->elem_count;
	}

	nk_clear(&ctx);

	glBindTexture(GL_TEXTURE_2D, 0);

	for (uint8 i = 0; i < NUM_GUI_VERTEX_ATTRIBUTES; i++) {
		glDisableVertexAttribArray(i);
	}

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);

	glUseProgram(0);

	return 0;
}

void shutdownGUI(void) {
	nk_free(&ctx);
	nk_buffer_free(&cmds);
	nk_font_atlas_clear(&atlas);

	glBindVertexArray(vertexArray);
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &indexBuffer);
	glBindVertexArray(0);

	glDeleteVertexArrays(1, &vertexArray);

	glDeleteTextures(1, &textureID);

	glDeleteProgram(shaderProgram);
}