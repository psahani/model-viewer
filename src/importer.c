#include "importer.h"
#include "log.h"

#include "file_utilities.h"
#include "renderer_utilities.h"
#include "entity_utilities.h"

#include "model-utility/material_exporter.h"
#include "model-utility/mesh_exporter.h"

#include "json-utilities/utilities.h"

#include "hash_map.h"
#include "list.h"

#include <GL/glu.h>

#define STBI_NO_PIC
#define STBI_NO_PNM

#include <stb/stb_image.h>

#include <sys/stat.h>

#include <dirent.h>
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define ASSET_BINARY_FILE_VERSION 1

#define SKELETON_FOLDER_NAME "skeleton"
#define MATERIALS_FOLDER "materials"

#define TEXTURES_BUCKET_COUNT 257
#define MATERIAL_FOLDERS_BUCKET_COUNT 101

Model model;
HashMap textures = NULL;

HashMap entityJoints = NULL;

internal HashMap materialFolders = NULL;

internal const char materialComponentCharacters[] = {
	'a', 'b', 'e', 'h', 'm', 'n', 'r'
};

#define NUM_TEXTURE_FILE_FORMATS 6
internal const char* textureFileFormats[NUM_TEXTURE_FILE_FORMATS] = {
	"tga", "png", "jpg", "bmp", "gif", "psd"
};

internal int32 exportModelData(void);
internal void copyModelFiles(const char *filename, const char *localFilename);
internal void moveModelFile(
	const char *filename,
	const char *suffix,
	const char *extension,
	const char *destinationFolder,
	bool reverse
);

internal int32 loadSubset(Subset *subset, FILE *assetFile, FILE *meshFile);
internal int32 loadMaterial(Material *material, FILE *file);
internal void loadMaterialFolders(UUID name);
internal int32 loadMask(Mask *mask, FILE *file);
internal int32 loadMaterialComponentTexture(
	UUID materialName,
	MaterialComponentType materialComponentType,
	UUID *textureName
);

internal int32 loadMaskTexture(char suffix, UUID *textureName);
internal int32 loadTexture(const char *filename, UUID name);
internal int32 loadAnimations(FILE *file);
internal int32 loadSkeletonJoints(HashMap joints);
internal void loadJoints(
	HashMap joints,
	HashMap entityJoints,
	Transform *parent,
	EntityJointTransform *entityJointTransform
);

internal void freeModel(void);
internal void freeSubset(Subset *subset);
internal void freeMaterial(Material *material);
internal void freeMask(Mask *mask);
internal void freeTexture(UUID name);
internal void freeAnimations(void);
internal void freeBone(Bone *bone);
internal void freeSkeleton(Skeleton *skeleton);

int32 importModel(bool export) {
	if (export && exportModelData() == -1) {
		return -1;
	}

	textures = createHashMap(
		sizeof(UUID),
		sizeof(Texture),
		TEXTURES_BUCKET_COUNT,
		(ComparisonOp)&strcmp
	);

	materialFolders = createHashMap(
		sizeof(UUID),
		sizeof(List),
		MATERIAL_FOLDERS_BUCKET_COUNT,
		(ComparisonOp)&strcmp
	);

	entityJoints = createHashMap(
		sizeof(UUID),
		sizeof(EntityJointTransform),
		JOINTS_BUCKET_COUNT,
		(ComparisonOp)&strcmp
	);

	if (strlen(model.name.string) == 0) {
		LOG("No models found\n");
		return 0;
	}

	int32 error = 0;

	LOG("Loading model (%s)...\n", model.name.string);

	char *modelFolder = getFullFilePath(model.name.string, NULL, MODEL_FOLDER);
	char *assetFolder = getFullFilePath("asset", NULL, modelFolder);
	free(modelFolder);

	char *assetFilename = getFullFilePath(
		model.name.string,
		"asset",
		assetFolder
	);

	FILE *assetFile = fopen(assetFilename, "rb");
	if (assetFile) {
		uint8 assetBinaryFileVersion;
		fread(&assetBinaryFileVersion, sizeof(uint8), 1, assetFile);

		if (assetBinaryFileVersion < ASSET_BINARY_FILE_VERSION) {
			LOG("WARNING: %s out of date\n", assetFilename);
			error = -1;
		}
	} else {
		error = -1;
	}

	if (error != -1) {
		char *meshFilename = getFullFilePath(
			model.name.string,
			"mesh",
			assetFolder
		);

		FILE *meshFile = fopen(meshFilename, "rb");
		if (meshFile) {
			uint8 meshBinaryFileVersion;
			fread(&meshBinaryFileVersion, sizeof(uint8), 1, meshFile);

			if (meshBinaryFileVersion < MESH_BINARY_FILE_VERSION) {
				LOG("WARNING: %s out of date\n", meshFilename);
				error = -1;
			}
		} else {
			error = -1;
		}

		if (error != -1) {
			fread(&model.numSubsets, sizeof(uint32), 1, assetFile);
			model.subsets = calloc(model.numSubsets, sizeof(Subset));

			for (uint32 i = 0; i < model.numSubsets; i++) {
				model.subsets[i].name = readStringAsUUID(assetFile);
			}

			error = loadMaskTexture('m', &model.materialTexture);
			if (error != -1) {
				error = loadMaskTexture('o', &model.opacityTexture);
			}

			if (error != -1) {
				for (uint32 i = 0; i < model.numSubsets; i++) {
					error = loadSubset(&model.subsets[i], assetFile, meshFile);
					if (error == -1) {
						break;
					}
				}

				if (error != -1) {
					error = loadAnimations(meshFile);
				}
			}
		} else {
			LOG("Failed to open %s\n", meshFilename);
			error = -1;
		}

		if (meshFile) {
			fclose(meshFile);
		}

		free(meshFilename);
	} else {
		LOG("Failed to open %s\n", assetFilename);
		error = -1;
	}

	if (assetFile) {
		fclose(assetFile);
	}

	free(assetFilename);
	free(assetFolder);

	if (error != -1) {
		LOG("Successfully loaded model (%s)\n", model.name.string);
	} else {
		LOG("Failed to load model (%s)\n", model.name.string);
	}

	return error;
}

int32 exportModelData(void) {
	DIR *dir = opendir(MODEL_FOLDER);
	if (dir) {
		memset(&model, 0, sizeof(Model));

		struct dirent *dirEntry = readdir(dir);
		while (dirEntry) {
			if (strcmp(dirEntry->d_name, ".") &&
				strcmp(dirEntry->d_name, "..")) {
				char *folderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					MODEL_FOLDER
				);

				struct stat info;
				stat(folderPath, &info);

				if (S_ISREG(info.st_mode)) {
					char *extension = getExtension(dirEntry->d_name);
					if (extension && !strcmp(extension, MODEL_FILE_EXTENSION)) {
						char *filename = removeExtension(folderPath);

						char *backslash = strrchr(filename, '/') + 1;
						UUID localFilename = stringToUUID(backslash);

						if (strlen(model.name.string) == 0) {
							model.name = localFilename;
						}

						if (access(filename, F_OK) == -1) {
							if (exportMaterials(
								filename,
								false,
								&logFunction) == -1) {
								closedir(dir);
								free(folderPath);
								free(extension);
								free(filename);
								return -1;
							}

							if (exportMesh(filename, &logFunction) == -1) {
								closedir(dir);
								free(folderPath);
								free(extension);
								free(filename);
								return -1;
							}

							if (exportAsset(filename, &logFunction) == -1) {
								closedir(dir);
								free(folderPath);
								free(extension);
								free(filename);
								return -1;
							}

							copyModelFiles(filename, localFilename.string);
						}

						free(filename);
					}

					free(extension);
				}

				free(folderPath);
			}

			dirEntry = readdir(dir);
		}

		closedir(dir);
	} else {
		LOG("Failed to open %s/\n", MODEL_FOLDER);
		return -1;
	}

	return 0;
}

int32 reloadModel(void) {
	freeResources();

	if (strlen(model.name.string) > 0) {
		char *modelFolder = getFullFilePath(
			model.name.string,
			NULL,
			MODEL_FOLDER
		);

		char *assetFolder = getFullFilePath(
			"asset",
			NULL,
			modelFolder
		);

		free(modelFolder);

		moveModelFile(
			model.name.string,
			NULL,
			"json",
			assetFolder,
			true
		);

		char *filename = getFullFilePath(
			model.name.string,
			NULL,
			MODEL_FOLDER
		);

		if (exportMaterials(filename, true, &logFunction) == -1) {
			memset(&model, 0, sizeof(Model));
			free(assetFolder);
			free(filename);
			return -1;
		}

		if (exportMesh(filename, &logFunction) == -1) {
			memset(&model, 0, sizeof(Model));
			free(assetFolder);
			free(filename);
			return -1;
		}

		if (exportAsset(filename, &logFunction) == -1) {
			memset(&model, 0, sizeof(Model));
			free(assetFolder);
			free(filename);
			return -1;
		}

		free(assetFolder);

		copyModelFiles(filename, model.name.string);

		free(filename);
	}

	return importModel(false);
}

void copyModelFiles(const char *filename, const char *localFilename) {
	deleteFolder(filename, false, &logFunction);

	char *entityFolder = getFullFilePath(
		"entity",
		NULL,
		filename
	);

	char *assetFolder = getFullFilePath(
		"asset",
		NULL,
		filename
	);

	MKDIR(filename);
	MKDIR(entityFolder);
	MKDIR(assetFolder);

	moveModelFile(
		localFilename,
		"_entity",
		"json",
		entityFolder,
		false
	);

	char *entityFilename = concatenateStrings(localFilename, "_", "entity");
	renameFile(
		entityFilename,
		"json",
		entityFolder,
		localFilename,
		"json",
		&logFunction
	);

	free(entityFilename);

	moveModelFile(
		localFilename,
		NULL,
		"json",
		assetFolder,
		false
	);

	moveModelFile(
		localFilename,
		NULL,
		"mesh",
		assetFolder,
		false
	);

	moveModelFile(
		localFilename,
		NULL,
		"asset",
		assetFolder,
		false
	);

	char *collisionPrimitivesFolder = getFullFilePath(
		COLLISION_PRIMITIVES_FOLDER_NAME,
		NULL,
		MODEL_FOLDER
	);

	if (access(collisionPrimitivesFolder, F_OK) != -1) {
		char *destinationCollisionPrimitivesFolder =
			getFullFilePath(
				COLLISION_PRIMITIVES_FOLDER_NAME,
				NULL,
				entityFolder
			);

		copyFolder(
			collisionPrimitivesFolder,
			destinationCollisionPrimitivesFolder,
			&logFunction
		);

		deleteFolder(collisionPrimitivesFolder, true, &logFunction);

		free(destinationCollisionPrimitivesFolder);
	}

	free(collisionPrimitivesFolder);

	char *skeletonFolder = getFullFilePath(
		SKELETON_FOLDER_NAME,
		NULL,
		MODEL_FOLDER
	);

	if (access(skeletonFolder, F_OK) != -1) {
		char *destinationSkeletonFolder =
			getFullFilePath(
				SKELETON_FOLDER_NAME,
				NULL,
				entityFolder
			);

		copyFolder(
			skeletonFolder,
			destinationSkeletonFolder,
			&logFunction
		);

		deleteFolder(skeletonFolder, true, &logFunction);

		free(destinationSkeletonFolder);
	}

	free(skeletonFolder);

	free(entityFolder);
	free(assetFolder);
}

void moveModelFile(
	const char *filename,
	const char *suffix,
	const char *extension,
	const char *destinationFolder,
	bool reverse
) {
	char *fullName = concatenateStrings(filename, NULL, suffix);
	char *fullFilename = getFullFilePath(fullName, extension, NULL);
	free(fullName);

	char *sourceFile = getFullFilePath(fullFilename, NULL, MODEL_FOLDER);
	char *destinationFile = getFullFilePath(
		fullFilename,
		NULL,
		destinationFolder
	);

	free(fullFilename);

	if (reverse) {
		copyFile(destinationFile, sourceFile, &logFunction);
		remove(destinationFile);
	} else {
		copyFile(sourceFile, destinationFile, &logFunction);
		remove(sourceFile);
	}

	free(sourceFile);
	free(destinationFile);
}

int32 loadSubset(Subset *subset, FILE *assetFile, FILE *meshFile) {
	LOG("Loading subset (%s)...\n", subset->name.string);

	if (loadMaterial(&subset->material, assetFile) == -1) {
		return -1;
	}

	if (loadMask(&subset->mask, assetFile) == -1) {
		return -1;
	}

	if (loadMesh(&subset->mesh, meshFile) == -1) {
		return -1;
	}

	LOG("Successfully loaded subset (%s)\n", subset->name.string);

	return 0;
}

int32 loadMaterial(Material *material, FILE *file) {
	for (uint32 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		MaterialComponent *materialComponent = &material->components[i];
		kmVec3Fill(&materialComponent->value, 1.0f, 1.0f, 1.0f);
	}

	material->name = readStringAsUUID(file);

	if (strlen(material->name.string) > 0) {
		LOG("Loading material (%s)...\n", material->name.string);

		fread(&material->doubleSided, sizeof(bool), 1, file);

		loadMaterialFolders(material->name);

		for (uint32 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
			MaterialComponent *materialComponent = &material->components[i];
			MaterialComponentType materialComponentType =
				(MaterialComponentType)i;

			if (loadMaterialComponentTexture(
				material->name,
				materialComponentType,
				&materialComponent->texture) == -1) {
				return -1;
			}

			fread(&materialComponent->value.x, sizeof(uint32), 1, file);
			fread(&materialComponent->value.y, sizeof(uint32), 1, file);
			fread(&materialComponent->value.z, sizeof(uint32), 1, file);
		}

		LOG("Successfully loaded material (%s)\n", material->name.string);
	}

	return 0;
}

void loadMaterialFolders(UUID name) {
	if (!hashMapGetData(materialFolders, &name)) {
		List materialNames = createList(sizeof(UUID));

		UUID fullMaterialName = name;
		UUID materialName = stringToUUID(strtok(fullMaterialName.string, "_"));
		while (strlen(materialName.string) > 0) {
			listPushBack(&materialNames, &materialName);
			materialName = stringToUUID(strtok(NULL, "_"));
		}

		List materialFoldersList = createList(sizeof(MaterialFolder));

		char *materialFolderPath = NULL;
		fullMaterialName = stringToUUID("");
		for (ListIterator itr = listGetIterator(&materialNames);
			!listIteratorAtEnd(itr);
			listMoveIterator(&itr)
		) {
			materialName = *LIST_ITERATOR_GET_ELEMENT(UUID, itr);
			if (!materialFolderPath) {
				materialFolderPath = malloc(strlen(materialName.string) + 3);
				sprintf(materialFolderPath, "m_%s", materialName.string);

				MaterialFolder materialFolder;

				materialFolder.folder = malloc(strlen(materialFolderPath) + 1);
				strcpy(materialFolder.folder, materialFolderPath);

				fullMaterialName = materialName;
				materialFolder.name = fullMaterialName;

				listPushFront(&materialFoldersList, &materialFolder);
			} else {
				MaterialFolder materialFolder;

				name = fullMaterialName;
				sprintf(
					fullMaterialName.string,
					"%s_%s",
					name.string,
					materialName.string
				);

				materialFolder.name = fullMaterialName;

				char *folder = malloc(strlen(materialFolderPath) + 1);
				strcpy(folder, materialFolderPath);

				materialFolderPath = realloc(
					materialFolderPath,
					strlen(materialFolderPath) +
					strlen(fullMaterialName.string) + 4
				);

				sprintf(
					materialFolderPath,
					"%s/m_%s",
					folder,
					fullMaterialName.string
				);

				free(folder);

				materialFolder.folder = malloc(strlen(materialFolderPath) + 1);
				strcpy(materialFolder.folder, materialFolderPath);

				listPushFront(&materialFoldersList, &materialFolder);
			}
		}

		listClear(&materialNames);
		free(materialFolderPath);

		hashMapInsert(
			materialFolders,
			&fullMaterialName,
			&materialFoldersList
		);
	}
}

int32 loadMask(Mask *mask, FILE *file) {
	LOG("Loading masks...\n");

	loadMaterial(&mask->collectionMaterial, file);
	loadMaterial(&mask->grungeMaterial, file);
	loadMaterial(&mask->wearMaterial, file);
	fread(&mask->opacity, sizeof(real32), 1, file);

	LOG("Successfully loaded masks\n");

	return 0;
}

int32 loadMesh(Mesh *mesh, FILE *file) {
	LOG("Loading mesh...\n");
	LOG("Loading mesh data...\n");

	uint32 numVertices;
	fread(&numVertices, sizeof(uint32), 1, file);

	Vertex *vertices = calloc(numVertices, sizeof(Vertex));
	fread(vertices, numVertices, sizeof(Vertex), file);

	uint32 numIndices;
	fread(&numIndices, sizeof(uint32), 1, file);

	uint32 *indices = calloc(numIndices, sizeof(uint32));
	fread(indices, numIndices, sizeof(uint32), file);

	LOG("Successfully loaded mesh data\n");
	LOG("Transferring mesh data onto GPU...\n");

	glGenBuffers(1, &mesh->vertexBuffer);
	glGenVertexArrays(1, &mesh->vertexArray);

	uint32 bufferIndex = 0;

	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(Vertex) * numVertices,
		vertices,
		GL_STATIC_DRAW
	);

	glBindVertexArray(mesh->vertexArray);
	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, position)
	);

	glVertexAttribPointer(
		bufferIndex++,
		4,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, color)
	);

	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, normal)
	);

	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, tangent)
	);

	glVertexAttribPointer(
		bufferIndex++,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, bitangent)
	);

	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, materialUV)
	);

	glVertexAttribPointer(
		bufferIndex++,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, maskUV)
	);

	glVertexAttribIPointer(
		bufferIndex++,
		NUM_BONES,
		GL_UNSIGNED_INT,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, bones)
	);

	glVertexAttribPointer(
		bufferIndex++,
		NUM_BONES,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(GLvoid*)offsetof(Vertex, weights)
	);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenBuffers(1, &mesh->indexBuffer);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		sizeof(uint32) * numIndices,
		indices,
		GL_STATIC_DRAW
	);

	mesh->numIndices = numIndices;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	free(vertices);
	free(indices);

	LOG("Successfully transferred mesh data onto GPU\n");
	LOG("Vertex Count: %d\n", numVertices);
	LOG("Triangle Count: %d\n", numIndices / 3);
	LOG("Successfully loaded mesh\n");

	return 0;
}

int32 loadAnimations(FILE *file) {
	bool hasAnimations;
	fread(&hasAnimations, sizeof(bool), 1, file);

	Skeleton *skeleton = &model.skeleton;
	skeleton->joints = createHashMap(
		sizeof(UUID),
		sizeof(Transform),
		JOINTS_BUCKET_COUNT,
		(ComparisonOp)&strcmp
	);

	if (hasAnimations) {
		fread(&skeleton->numBoneOffsets, sizeof(uint32), 1, file);
		skeleton->boneOffsets = calloc(
			skeleton->numBoneOffsets,
			sizeof(BoneOffset)
		);

		for (uint32 i = 0; i < skeleton->numBoneOffsets; i++) {
			BoneOffset *boneOffset = &skeleton->boneOffsets[i];
			boneOffset->name = readUUID(file);
			boneOffset->transform = readTransform(file);
		}

		fread(&model.numAnimations, sizeof(uint32), 1, file);
		model.animations = calloc(model.numAnimations, sizeof(Animation));

		for (uint32 i = 0; i < model.numAnimations; i++) {
			Animation *animation = &model.animations[i];

			animation->name = readUUID(file);
			fread(&animation->fps, sizeof(real64), 1, file);

			fread(&animation->numBones, sizeof(uint32), 1, file);
			animation->bones = calloc(animation->numBones, sizeof(Bone));

			for (uint32 j = 0; j < animation->numBones; j++) {
				Bone *bone = &animation->bones[j];

				bone->name = readUUID(file);

				fread(&bone->numPositionKeyFrames, sizeof(uint32), 1, file);
				bone->positionKeyFrames = calloc(
					bone->numPositionKeyFrames,
					sizeof(Vec3KeyFrame)
				);

				for (uint32 k = 0; k < bone->numPositionKeyFrames; k++) {
					Vec3KeyFrame *keyFrame = &bone->positionKeyFrames[k];
					fread(&keyFrame->time, sizeof(real64), 1, file);
					fread(&keyFrame->value, sizeof(kmVec3), 1, file);
				}

				fread(&bone->numRotationKeyFrames, sizeof(uint32), 1, file);
				bone->rotationKeyFrames = calloc(
					bone->numRotationKeyFrames,
					sizeof(QuaternionKeyFrame)
				);

				for (uint32 k = 0; k < bone->numRotationKeyFrames; k++) {
					QuaternionKeyFrame *keyFrame = &bone->rotationKeyFrames[k];
					fread(&keyFrame->time, sizeof(real64), 1, file);
					fread(&keyFrame->value, sizeof(kmQuaternion), 1, file);
				}

				fread(&bone->numScaleKeyFrames, sizeof(uint32), 1, file);
				bone->scaleKeyFrames = calloc(
					bone->numScaleKeyFrames,
					sizeof(Vec3KeyFrame)
				);

				for (uint32 k = 0; k < bone->numScaleKeyFrames; k++) {
					Vec3KeyFrame *keyFrame = &bone->scaleKeyFrames[k];
					fread(&keyFrame->time, sizeof(real64), 1, file);
					fread(&keyFrame->value, sizeof(kmVec3), 1, file);
				}
			}

			fread(&animation->duration, sizeof(real64), 1, file);
		}

		if (loadSkeletonJoints(skeleton->joints) == -1) {
			return -1;
		}
	} else {
		model.numAnimations = 0;
	}

	return 0;
}

int32 loadSkeletonJoints(HashMap joints) {
	int32 error = 0;

	char *modelFolder = getFullFilePath(model.name.string, NULL, MODEL_FOLDER);
	char *entityFolder = getFullFilePath("entity", NULL, modelFolder);
	free(modelFolder);

	char *skeletonFolder = getFullFilePath(
		SKELETON_FOLDER_NAME,
		NULL,
		entityFolder
	);

	char *entityFilename = getFullFilePath(
		model.name.string,
		NULL,
		entityFolder
	);

	cJSON *json = loadJSON(entityFilename, &logFunction);
	free(entityFilename);

	cJSON *components = cJSON_GetObjectItem(json, "components");

	cJSON *animation = cJSON_GetObjectItem(components, "animation");

	cJSON *skeletonUUID = animation->child->child->next;
	UUID skeletonID = stringToUUID(skeletonUUID->valuestring);

	cJSON_Delete(json);

	free(entityFolder);

	hashMapClear(entityJoints);

	DIR *dir = opendir(skeletonFolder);
	if (dir) {
		struct dirent *dirEntry = readdir(dir);
		while (dirEntry) {
			if (strcmp(dirEntry->d_name, ".") &&
				strcmp(dirEntry->d_name, "..")) {
				char *folderPath = getFullFilePath(
					dirEntry->d_name,
					NULL,
					skeletonFolder
				);

				struct stat info;
				stat(folderPath, &info);

				if (S_ISREG(info.st_mode)) {
					char *filename = removeExtension(folderPath);

					json = loadJSON(filename, &logFunction);

					if (json) {
						EntityJointTransform entityJointTransform;

						components = cJSON_GetObjectItem(json, "components");

						cJSON *jointName = cJSON_GetObjectItem(
							components,
							"joint"
						)->child->child->next;

						entityJointTransform.name = stringToUUID(
							jointName->valuestring
						);

						cJSON *transform = cJSON_GetObjectItem(
							components,
							"transform"
						);

						cJSON *position = transform->child;
						getFloatArray(
							position->child->next,
							3,
							(real32*)&entityJointTransform.position
						);

						cJSON *rotation = position->next;
						getFloatArray(
							rotation->child->next,
							4,
							(real32*)&entityJointTransform.rotation
						);

						cJSON *scale = rotation->next;
						getFloatArray(
							scale->child->next,
							3,
							(real32*)&entityJointTransform.scale
						);

						cJSON *parent = scale->next;
						entityJointTransform.parent = stringToUUID(
							parent->child->next->valuestring
						);

						cJSON *firstChild = parent->next;
						entityJointTransform.firstChild = stringToUUID(
							firstChild->child->next->valuestring
						);

						cJSON *nextSibling = firstChild->next;
						entityJointTransform.nextSibling = stringToUUID(
							nextSibling->child->next->valuestring
						);

						cJSON *jsonUUID = cJSON_GetObjectItem(json, "uuid");
						UUID uuid = stringToUUID(jsonUUID->valuestring);

						hashMapInsert(
							entityJoints,
							&uuid,
							&entityJointTransform
						);

						cJSON_Delete(json);
					}

					free(filename);
				}

				free(folderPath);
			}

			dirEntry = readdir(dir);
		}

		closedir(dir);
	} else {
		LOG("Failed to open %s/\n", skeletonFolder);
		error = -1;
	}

	free(skeletonFolder);

	if (error != -1) {
		EntityJointTransform *rootJointTransform = hashMapGetData(
			entityJoints,
			&skeletonID
		);

		loadJoints(joints, entityJoints, NULL, rootJointTransform);
	}

	return error;
}

void loadJoints(
	HashMap joints,
	HashMap entityJoints,
	Transform *parent,
	EntityJointTransform *entityJointTransform
) {
	Transform transform = createTransform(
		&entityJointTransform->position,
		&entityJointTransform->rotation,
		&entityJointTransform->scale
	);

	transform.parent = parent;

	hashMapInsert(joints, &entityJointTransform->name, &transform);

	Transform *transformReference = hashMapGetData(
		joints,
		&entityJointTransform->name
	);

	if (parent) {
		addChildToTransform(parent, transformReference);
	} else {
		model.skeleton.rootJoint = transformReference;
	}

	UUID child = entityJointTransform->firstChild;
	do {
		EntityJointTransform *entityJointTransform = hashMapGetData(
			entityJoints,
			&child
		);

		if (entityJointTransform) {
			loadJoints(
				joints,
				entityJoints,
				transformReference,
				entityJointTransform
			);

			child = entityJointTransform->nextSibling;
		} else {
			break;
		}
	} while (true);
}

int32 loadMaterialComponentTexture(
	UUID materialName,
	MaterialComponentType materialComponentType,
	UUID *textureName
) {
	memset(textureName, 0, sizeof(UUID));

	List *materialFoldersList = (List*)hashMapGetData(
		materialFolders,
		&materialName
	);

	char *fullFilename = NULL;
	for (ListIterator itr = listGetIterator(materialFoldersList);
		!listIteratorAtEnd(itr);
		listMoveIterator(&itr)
	) {
		MaterialFolder *materialFolder =
			LIST_ITERATOR_GET_ELEMENT(MaterialFolder, itr);

		char *filename = malloc(
			strlen(materialFolder->folder) +
			strlen(materialFolder->name.string) + 16
		);

		sprintf(
			filename,
			"materials/%s/t_%s_%c",
			materialFolder->folder,
			materialFolder->name.string,
			materialComponentCharacters[materialComponentType]
		);

		fullFilename = getFullTextureFilename(filename);
		free(filename);

		if (fullFilename) {
			sprintf(
				textureName->string,
				"%s_%c",
				materialFolder->name.string,
				materialComponentCharacters[materialComponentType]
			);

			break;
		}

		free(fullFilename);
	}

	if (fullFilename) {
		if (loadTexture(fullFilename, *textureName) == -1) {
			free(fullFilename);
			return -1;
		}

		free(fullFilename);
	}

	return 0;
}

int32 loadMaskTexture(char suffix, UUID *textureName) {
	memset(textureName, 0, sizeof(UUID));
	sprintf(textureName->string, "%s_%c", model.name.string, suffix);

	char *filename = malloc(strlen(textureName->string) + 10);
	sprintf(filename, "masks/mt_%s", textureName->string);

	char *fullFilename = getFullTextureFilename(filename);
	free(filename);

	if (fullFilename) {
		if (loadTexture(fullFilename, *textureName) == -1) {
			free(fullFilename);
			return -1;
		}

		free(fullFilename);
	} else {
		memset(textureName, 0, sizeof(UUID));
	}

	return 0;
}

char* getFullTextureFilename(const char *filename) {
	char *fullFilename = malloc(strlen(filename) + 5);
	for (uint32 i = 0; i < NUM_TEXTURE_FILE_FORMATS; i++) {
		sprintf(fullFilename, "%s.%s", filename, textureFileFormats[i]);
		if (access(fullFilename, F_OK) != -1) {
			return fullFilename;
		}
	}

	free(fullFilename);

	return NULL;
}

int32 loadTexture(const char *filename, UUID name) {
	if (!hashMapGetData(textures, &name)) {
		char *textureName = strrchr(filename, '/') + 1;
		LOG("Loading texture (%s)...\n", textureName);

		Texture texture = {};
		texture.name = name;

		int32 width, height, numComponents;
		uint8 *data = stbi_load(filename, &width, &height, &numComponents, 4);

		numComponents = 4;

		if (!data) {
			LOG("Failed to load texture\n");
			return -1;
		}

		glGenTextures(1, &texture.id);
		glBindTexture(GL_TEXTURE_2D, texture.id);

		GLenum format = GL_RGBA;
		switch (numComponents) {
			case 1:
				format = GL_R;
				break;
			case 2:
				format = GL_RG;
				break;
			case 3:
				format = GL_RGB;
				break;
			default:
				break;
		}

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			format,
			width,
			height,
			0,
			format,
			GL_UNSIGNED_BYTE,
			data
		);

		free(data);

		int32 error = logGLError(false, "Failed to transfer texture onto GPU");

		if (error != -1) {
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR
			);

			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_MAG_FILTER,
				GL_LINEAR
			);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			if (checkGLExtension("GL_ARB_bindless_texture") &&
				checkGLExtension("GL_ARB_gpu_shader_int64")
			) {
				texture.handle = glGetTextureHandleARB(texture.id);
				glMakeTextureHandleResidentARB(texture.handle);
			}
		}

		glBindTexture(GL_TEXTURE_2D, 0);

		hashMapInsert(textures, &name, &texture);

		LOG("Successfully loaded texture (%s)\n", textureName);
		LOG("Texture Count: %d\n", textures->count);
	}

	return 0;
}

void freeResources(void) {
	freeModel();
	freeAnimations();

	if (textures) {
		freeHashMap(&textures);
	}

	if (materialFolders) {
		for (HashMapIterator itr = hashMapGetIterator(materialFolders);
			!hashMapIteratorAtEnd(itr);
			hashMapMoveIterator(&itr)
		) {
			List *materialFoldersList = (List*)hashMapIteratorGetValue(itr);
			for (ListIterator listItr = listGetIterator(materialFoldersList);
				!listIteratorAtEnd(listItr);
				listMoveIterator(&listItr)
			) {
				free(LIST_ITERATOR_GET_ELEMENT(
					MaterialFolder,
					listItr
				)->folder);
			}

			listClear(materialFoldersList);
		}

		freeHashMap(&materialFolders);
	}

	if (entityJoints) {
		freeHashMap(&entityJoints);
	}
}

void freeModel(void) {
	if (strlen(model.name.string) > 0) {
		LOG("Freeing model (%s)...\n", model.name.string);
	}

	freeTexture(model.materialTexture);
	freeTexture(model.opacityTexture);

	for (uint32 i = 0; i < model.numSubsets; i++) {
		freeSubset(&model.subsets[i]);
	}

	free(model.subsets);

	if (strlen(model.name.string) > 0) {
		LOG("Successfully freed model (%s)\n", model.name.string);
	}
}

void freeSubset(Subset *subset) {
	freeMesh(&subset->mesh);
	freeMaterial(&subset->material);
	freeMask(&subset->mask);
}

void freeMesh(Mesh *mesh) {
	glBindVertexArray(mesh->vertexArray);
	glDeleteBuffers(1, &mesh->vertexBuffer);
	glDeleteBuffers(1, &mesh->indexBuffer);
	glBindVertexArray(0);

	glDeleteVertexArrays(1, &mesh->vertexArray);
}

void freeMaterial(Material *material) {
	for (uint32 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		freeTexture(material->components[i].texture);
	}
}

void freeMask(Mask *mask) {
	freeMaterial(&mask->collectionMaterial);
	freeMaterial(&mask->grungeMaterial);
	freeMaterial(&mask->wearMaterial);
}

void freeTexture(UUID name) {
	if (textures) {
		Texture *texture = (Texture*)hashMapGetData(textures, &name);
		if (texture) {
			LOG("Freeing texture (%s)...\n", name.string);

			if (checkGLExtension("GL_ARB_bindless_texture") &&
				checkGLExtension("GL_ARB_gpu_shader_int64")
			) {
				glMakeTextureHandleNonResidentARB(texture->handle);
			}

			glDeleteTextures(1, &texture->id);
			hashMapDelete(textures, &name);

			LOG("Successfully freed texture (%s)\n", name.string);
			LOG("Texture Count: %d\n", textures->count);
		}
	}
}

void freeAnimations(void) {
	freeSkeleton(&model.skeleton);

	for (uint32 i = 0; i < model.numAnimations; i++) {
		Animation *animation = &model.animations[i];
		for (uint32 j = 0; j < animation->numBones; j++) {
			freeBone(&animation->bones[j]);
		}

		free(animation->bones);
	}

	free(model.animations);
}

void freeBone(Bone *bone) {
	free(bone->positionKeyFrames);
	free(bone->rotationKeyFrames);
	free(bone->scaleKeyFrames);
}

void freeSkeleton(Skeleton *skeleton) {
	free(skeleton->boneOffsets);

	if (skeleton->joints) {
		for (HashMapIterator itr = hashMapGetIterator(skeleton->joints);
			!hashMapIteratorAtEnd(itr);
			hashMapMoveIterator(&itr)
		) {
			freeTransform(hashMapIteratorGetValue(itr));
		}

		freeHashMap(&skeleton->joints);
	}
}