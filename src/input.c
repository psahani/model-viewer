#include "input.h"
#include "animator.h"

#include <kazmath/vec2.h>
#include <kazmath/vec3.h>

#include <string.h>

internal bool mouseHeldDown[3];
internal bool speedUpButtonHeldDown;
internal kmVec2 previousCursorPosition;

extern void updateCameraRotationAcceleration(real32 x, real32 y);
extern void updateCameraPositionAcceleration(real32 x, real32 y, real32 z);
extern void updateCameraTranslationAcceleration(real32 x, real32 y, real32 z);
extern void updateProjection(real32 fov, real32 nearPlane, real32 farPlane);
extern void reloadViewer(void);
extern void toggleFullscreen(void);
extern void toggleWireframe(void);
extern void toggleCollisionPrimitives(void);
extern void toggleLight(void);
extern void toggleGrid(void);

internal void keyCallback(
	GLFWwindow *window,
	int32 key,
	int32 scancode,
	int32 action,
	int32 mods
) {
	if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESCAPE:
				glfwSetWindowShouldClose(window, 1);
				break;
			case GLFW_KEY_R:
				reloadViewer();
				break;
			case GLFW_KEY_F:
				toggleFullscreen();
				break;
			case GLFW_KEY_W:
				toggleWireframe();
				break;
			case GLFW_KEY_C:
				toggleCollisionPrimitives();
				break;
			case GLFW_KEY_L:
				toggleLight();
				break;
			case GLFW_KEY_G:
				toggleGrid();
				break;
			case GLFW_KEY_P:
				toggleAnimatorPaused();
				break;
			case GLFW_KEY_LEFT:
				switchAnimation(true);
				break;
			case GLFW_KEY_RIGHT:
				switchAnimation(false);
				break;
			case GLFW_KEY_UP:
				adjustAnimatorSpeed(ANIMATION_SPEED_SENSITIVITY);
				break;
			case GLFW_KEY_DOWN:
				adjustAnimatorSpeed(-ANIMATION_SPEED_SENSITIVITY);
				break;
			case GLFW_KEY_O:
				restartAnimator();
				break;
			case GLFW_KEY_I:
				reverseAnimator();
				break;
			case GLFW_KEY_Q:
				speedUpButtonHeldDown = true;
				break;
			default:
				break;
		}
	} else if (action == GLFW_RELEASE) {
		switch (key) {
			case GLFW_KEY_Q:
				speedUpButtonHeldDown = false;
				break;
			default:
				break;
		}
	}
}

internal void cursorPositionCallback(
	GLFWwindow *window,
	real64 xpos,
	real64 ypos
) {
	kmVec2 delta;
	kmVec2Fill(
		&delta,
		xpos - previousCursorPosition.x,
		ypos - previousCursorPosition.y
	);

	if (mouseHeldDown[0]) {
		updateCameraRotationAcceleration(
			delta.x * -X_ROTATION_SENSITIVITY,
			delta.y * -Y_ROTATION_SENSITIVITY
		);
	} else if (mouseHeldDown[1]) {
		updateCameraTranslationAcceleration(
			delta.x * -X_TRANSLATION_SENSITIVITY,
			0.0f,
			delta.y * -Z_TRANSLATION_SENSITIVITY
		);
	} else if (mouseHeldDown[2]) {
		updateCameraTranslationAcceleration(
			0.0f,
			delta.y * Y_TRANSLATION_SENSITIVITY,
			0.0f
		);
	}

	previousCursorPosition.x = xpos;
	previousCursorPosition.y = ypos;
}

internal void mouseButtonCallback(
	GLFWwindow *window,
	int32 button,
	int32 action,
	int32 mods
) {
	int32 buttonIndex = -1;
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			buttonIndex = 0;
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			buttonIndex = 1;
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			buttonIndex = 2;
			break;
		default:
			break;
	}

	if (buttonIndex == -1) {
		return;
	}

	switch (action) {
		case GLFW_PRESS:
			mouseHeldDown[buttonIndex] = true;
			break;
		case GLFW_RELEASE:
			mouseHeldDown[buttonIndex] = false;
			break;
		default:
			break;
	}
}

internal void mouseScrollCallback(
	GLFWwindow *window,
	real64 xoffset,
	real64 yoffset
) {
	updateCameraPositionAcceleration(0.0f, 0.0f, yoffset * -ZOOM_SENSITIVITY);
}

void initializeInput(GLFWwindow *window) {
	glfwSetKeyCallback(window, &keyCallback);
	glfwSetCursorPosCallback(window, &cursorPositionCallback);
	glfwSetMouseButtonCallback(window, &mouseButtonCallback);
	glfwSetScrollCallback(window, &mouseScrollCallback);

	memset(mouseHeldDown, 0, 3 * sizeof(bool));
	speedUpButtonHeldDown = false;
	previousCursorPosition = KM_VEC2_ZERO;
}