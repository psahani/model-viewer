#include "definitions.h"

#include "importer.h"
#include "cubemap_exporter.h"
#include "renderer.h"
#include "debug_renderer.h"
#include "collision_primitive_renderer.h"
#include "cubemap_renderer.h"
#include "animator.h"
#include "input.h"
#include "gui.h"
#include "log.h"
#include "config.h"

#include <GL/glew.h>
#include <GL/glu.h>

#include <GLFW/glfw3.h>

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

internal GLFWwindow *window;
internal bool fullscreen;
internal int32 x, y, w, h;

#define FRAME_RATE 60.0

#define FOV 80.0f
#define NEAR_PLANE 0.01f
#define FAR_PLANE 1000.0f

#define CAMERA_ROTATION_RESTITUTION 5.0f
#define CAMERA_POSITION_RESTITUTION 5.0f
#define CAMERA_TRANSLATION_RESTITUTION 5.0f

internal Camera camera;

internal kmVec2 cameraPitchYaw;
internal kmVec2 cameraRotationVelocity;
internal kmVec2 cameraRotationAcceleration;

internal kmVec3 cameraPositionVelocity;
internal kmVec3 cameraPositionAcceleration;

internal kmVec3 cameraTranslationVelocity;
internal kmVec3 cameraTranslationAcceleration;

internal bool wireframe = true;
internal bool collisionPrimitives = true;
internal bool light = true;
internal bool grid = true;

extern Config config;

real64 dt;

internal GLFWmonitor* getActiveMonitor(void);

internal void errorCallback(int error, const char *description);
internal GLFWwindow* initializeWindow(
	uint32 width,
	uint32 height,
	const char *title
);
internal int32 freeWindow(GLFWwindow *window);

internal void initializeCamera(void);
internal void updateCamera(void);

void toggleFullscreen(void);

int32 main(int32 argc, char *argv[]) {
	if (loadConfig() == -1) {
		LOG("Using default configuration\n");
	}

	srand(time(0));

	if (LOG_FILE_NAME) {
		remove(LOG_FILE_NAME);
	}

	window = initializeWindow(
		config.windowConfig.size.x,
		config.windowConfig.size.y,
		config.windowConfig.title
	);

	if (!window) {
		return -1;
	}

	if (config.windowConfig.fullscreen) {
		toggleFullscreen();
	}

	initializeCubemapExporter();

	if (importModel(true) == -1) {
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(config.windowConfig.vsync);

	if (initializeRenderer() == -1) {
		freeWindow(window);
		return -1;
	}

	if (initializeDebugRenderer() == -1) {
		freeWindow(window);
		return -1;
	}

	if (initializeCollisionPrimitiveRenderer() == -1) {
		freeWindow(window);
		return -1;
	}

	if (initializeCubemapRenderer() == -1) {
		freeWindow(window);
		return -1;
	}

	// if (initializeGUI() == -1) {
	// 	freeWindow(window);
	// 	return -1;
	// }

	initializeAnimator();

	initializeInput(window);
	initializeCamera();

	kmVec3 scale;
	kmVec3Fill(&scale, 1.0f, 1.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glClearColor(
		config.graphicsConfig.backgroundColor.x,
		config.graphicsConfig.backgroundColor.y,
		config.graphicsConfig.backgroundColor.z,
		1.0f
	);

	// real64 t = 0.0;
	dt = 1.0 / FRAME_RATE;

	real64 currentTime = glfwGetTime();
	real64 accumulator = 0.0;

	while(!glfwWindowShouldClose(window)) {
		real64 newTime = glfwGetTime();
		real64 frameTime = newTime - currentTime;
		if (frameTime > 0.25) {
			frameTime = 0.25;
		}

		currentTime = newTime;
		accumulator += frameTime;

		while (accumulator >= dt) {
			// t += dt;
			accumulator -= dt;

			glfwPollEvents();
			updateAnimator();
			// updateGUI(window);
		}

		updateCamera();

		int32 width, height;
		glfwGetFramebufferSize(window, &width, &height);

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (width == 0 || height == 0) {
			glfwSwapBuffers(window);
			continue;
		}

		camera.aspectRatio = (real32)width / (real32)height;

		if (drawCubemap(&camera) == -1) {
			break;
		}

		if (drawModel(&camera, scale, wireframe, light) == -1) {
			break;
		}

		if (grid) {
			drawGrid(
				config.viewerConfig.gridDimensions[1],
				config.viewerConfig.gridDimensions[0],
				config.viewerConfig.gridCellSize,
				&config.viewerConfig.gridColor
			);
		}

		if (drawDebugLines(&camera) == -1) {
			break;
		}

		if (collisionPrimitives && drawCollisionPrimitives(&camera) == -1) {
			break;
		}

		// int32 windowWidth, windowHeight;
		// glfwGetWindowSize(window, &windowWidth, &windowHeight);

		// kmVec2 framebufferScale;
		// kmVec2Fill(
		// 	&framebufferScale,
		// 	(real32)width / (real32)windowWidth,
		// 	(real32)height / (real32)windowHeight
		// );

		// if (drawGUI(
		// 	framebufferScale,
		// 	windowWidth,
		// 	windowHeight) == -1) {
		// 	break;
		// }

		glfwSwapBuffers(window);
	}

	// shutdownGUI();
	shutdownRenderer();
	shutdownDebugRenderer();
	shutdownCollisionPrimitiveRenderer();
	shutdownCubemapRenderer();

	freeResources();
	shutdownCubemapExporter();
	freeWindow(window);
	freeConfig();

	return 0;
}

GLFWmonitor* getActiveMonitor(void) {
	GLFWmonitor *activeMonitor = NULL;

	glfwGetWindowPos(window, &x, &y);
	glfwGetWindowSize(window, &w, &h);

	int32 numMonitors;
	GLFWmonitor **monitors = glfwGetMonitors(&numMonitors);

	int32 maxOverlap = 0;
	for (uint32 i = 0; i < numMonitors; i++) {
		GLFWmonitor *monitor = monitors[i];

		int32 mX, mY;
		glfwGetMonitorPos(monitor, &mX, &mY);

		const GLFWvidmode *mode = glfwGetVideoMode(monitor);

		int32 overlap =
			MAX(0, MIN(x + w, mX + mode->width) - MAX(x, mX)) *
			MAX(0, MIN(y + h, mY + mode->height) - MAX(y, mY));

		if (overlap > maxOverlap) {
			maxOverlap = overlap;
			activeMonitor = monitor;
		}
	}

	if (!activeMonitor) {
		activeMonitor = glfwGetPrimaryMonitor();
	}

	return activeMonitor;
}

void errorCallback(int error, const char *description) {
	LOG("GLFW Error %d: %s\n", error, description);
}

GLFWwindow* initializeWindow(uint32 width, uint32 height, const char *title) {
	if (!glfwInit()) {
		return NULL;
	}

	glfwSetErrorCallback(&errorCallback);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLFW_VERSION_MAJOR);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLFW_VERSION_MINOR);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, config.windowConfig.numMSAASamples);

	glfwWindowHint(GLFW_MAXIMIZED, config.windowConfig.maximized);
	glfwWindowHint(GLFW_RESIZABLE, config.windowConfig.resizable);

	GLFWwindow *window = glfwCreateWindow(width, height, title, NULL, NULL);
	if (!window) {
		glfwTerminate();
		return NULL;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	if (glewError != GLEW_OK) {
		LOG("Error: %s\n", glewGetErrorString(glewError));
	}

	return window;
}

int32 freeWindow(GLFWwindow *window) {
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}

void initializeCamera(void) {
	camera.fov = FOV;
	camera.nearPlane = NEAR_PLANE;
	camera.farPlane = FAR_PLANE;

	kmQuaternionIdentity(&camera.rotation);

	cameraPitchYaw = KM_VEC2_ZERO;
	cameraRotationVelocity = KM_VEC2_ZERO;
	cameraRotationAcceleration = KM_VEC2_ZERO;

	kmVec3Fill(&camera.position, 0.0f, 0.0f, 5.0f);

	cameraPositionVelocity = KM_VEC3_ZERO;
	cameraPositionAcceleration = KM_VEC3_ZERO;

	camera.translation = KM_VEC3_ZERO;
	cameraTranslationVelocity = KM_VEC3_ZERO;
	cameraTranslationAcceleration = KM_VEC3_ZERO;
}

void updateCamera(void) {
	kmVec2 vec2Zero = KM_VEC2_ZERO;
	kmVec3 vec3Zero = KM_VEC3_ZERO;

	kmVec2 deltaRotationVelocity;
	kmVec2Scale(&deltaRotationVelocity, &cameraRotationAcceleration, dt);
	kmVec2Add(
		&cameraRotationVelocity,
		&cameraRotationVelocity,
		&deltaRotationVelocity
	);

	cameraRotationAcceleration = KM_VEC2_ZERO;

	kmVec2 deltaPitchYaw;
	kmVec2Scale(&deltaPitchYaw, &cameraRotationVelocity, dt);
	kmVec2Add(&cameraPitchYaw, &cameraPitchYaw, &deltaPitchYaw);

	if (cameraPitchYaw.x < 0.0f) {
		cameraPitchYaw.x = 2 * kmPI;
	} else if (cameraPitchYaw.x > 2 * kmPI) {
		cameraPitchYaw.x = 0.0f;
	}

	if (cameraPitchYaw.y < -0.49f * kmPI) {
		cameraPitchYaw.y = -0.49f * kmPI;
	} else if (cameraPitchYaw.y > 0.49f * kmPI) {
		cameraPitchYaw.y = 0.49f * kmPI;
	}

	kmQuaternionRotationPitchYawRoll(
		&camera.rotation,
		cameraPitchYaw.y,
		cameraPitchYaw.x,
		0.0f
	);

	kmVec2Lerp(
		&cameraRotationVelocity,
		&cameraRotationVelocity,
		&vec2Zero,
		CAMERA_ROTATION_RESTITUTION * dt
	);

	kmVec3 deltaPositionVelocity;
	kmVec3Scale(&deltaPositionVelocity, &cameraPositionAcceleration, dt);
	kmVec3Add(
		&cameraPositionVelocity,
		&cameraPositionVelocity,
		&deltaPositionVelocity
	);

	cameraPositionAcceleration = KM_VEC3_ZERO;

	kmVec3 deltaPosition;
	kmVec3Scale(&deltaPosition, &cameraPositionVelocity, dt);
	kmVec3Add(&camera.position, &camera.position, &deltaPosition);

	if (camera.position.z < 0.01f) {
		camera.position.z = 0.01f;
	}

	kmVec3Lerp(
		&cameraPositionVelocity,
		&cameraPositionVelocity,
		&vec3Zero,
		CAMERA_POSITION_RESTITUTION * dt
	);

	kmVec3 deltaTranslationVelocity;
	kmVec3Scale(&deltaTranslationVelocity, &cameraTranslationAcceleration, dt);
	kmVec3Add(
		&cameraTranslationVelocity,
		&cameraTranslationVelocity,
		&deltaTranslationVelocity
	);

	cameraTranslationAcceleration = KM_VEC3_ZERO;

	kmVec3 deltaTranslation;
	kmVec3Scale(&deltaTranslation, &cameraTranslationVelocity, dt);

	kmVec3 right;
	kmQuaternionGetRightVec3(&right, &camera.rotation);

	kmVec3 up;
	kmVec3Fill(&up, 0.0f, 1.0f, 0.0f);

	kmVec3 forward;
	kmVec3Cross(&forward, &right, &up);

	right.y = 0.0f;
	kmVec3Normalize(&right, &right);
	kmVec3Scale(&right, &right, deltaTranslation.x);
	kmVec3Add(&camera.translation, &camera.translation, &right);

	forward.y = 0.0f;
	kmVec3Normalize(&forward, &forward);
	kmVec3Scale(&forward, &forward, deltaTranslation.z);
	kmVec3Add(&camera.translation, &camera.translation, &forward);

	camera.translation.y += deltaTranslation.y;

	kmVec3Lerp(
		&cameraTranslationVelocity,
		&cameraTranslationVelocity,
		&vec3Zero,
		CAMERA_TRANSLATION_RESTITUTION * dt
	);
}

void updateCameraRotationAcceleration(real32 x, real32 y) {
	kmVec2Fill(&cameraRotationAcceleration, x, y);
}

void updateCameraPositionAcceleration(real32 x, real32 y, real32 z) {
	kmVec3Fill(&cameraPositionAcceleration, x, y, z);
}

void updateCameraTranslationAcceleration(real32 x, real32 y, real32 z) {
	kmVec3Fill(&cameraTranslationAcceleration, x, y, z);
}

void updateProjection(real32 fov, real32 nearPlane, real32 farPlane) {
	camera.fov = fov;
	camera.nearPlane = nearPlane;
	camera.farPlane = farPlane;
}

void reloadViewer(void) {
	reloadModel();
	initializeAnimator();
}

void toggleFullscreen(void) {
	fullscreen = !fullscreen;

	if (fullscreen) {
		GLFWmonitor *monitor = getActiveMonitor();
		const GLFWvidmode *mode = glfwGetVideoMode(monitor);

		glfwSetWindowMonitor(
			window,
			monitor,
			0,
			0,
			mode->width,
			mode->height,
			GLFW_DONT_CARE
		);
	} else {
		glfwSetWindowMonitor(window, NULL, x, y, w, h, GLFW_DONT_CARE);
	}

	glfwSwapInterval(1 - config.windowConfig.vsync);
	glfwSwapInterval(config.windowConfig.vsync);
}

void toggleWireframe(void) {
	wireframe = !wireframe;
}

void toggleCollisionPrimitives(void) {
	collisionPrimitives = !collisionPrimitives;
}

void toggleLight(void) {
	light = !light;
}

void toggleGrid(void) {
	grid = !grid;
}