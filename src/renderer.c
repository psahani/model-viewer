#include "renderer.h"
#include "importer.h"
#include "config.h"
#include "log.h"

#include "renderer_utilities.h"
#include "cubemap_exporter.h"
#include "debug_renderer.h"
#include "hash_map.h"

#include <malloc.h>
#include <string.h>
#include <stdio.h>

#define VERTEX_SHADER_FILE "resources/shaders/model.vert"
#define FRAGMENT_SHADER_FILE "resources/shaders/model.frag"
#define PBR_FRAGMENT_SHADER_FILE "resources/shaders/model_pbr.frag"
#define PBR_FALLBACK_FRAGMENT_SHADER_FILE \
	"resources/shaders/model_pbr_fallback.frag"

internal bool fallbackShaders;

internal GLuint shaderProgram;

internal Uniform modelUniform;
internal Uniform viewUniform;
internal Uniform projectionUniform;

internal Uniform hasAnimationsUniform;
internal Uniform boneTransformsUniform;

internal Uniform materialActiveUniform;
internal Uniform materialUniform;
internal Uniform materialValuesUniform;

internal Uniform materialMaskUniform;

internal Uniform opacityMaskActiveUniform;
internal Uniform opacityMaskUniform;

internal Uniform collectionMaterialActiveUniform;
internal Uniform collectionMaterialUniform;
internal Uniform collectionMaterialValuesUniform;

internal Uniform grungeMaterialActiveUniform;
internal Uniform grungeMaterialUniform;
internal Uniform grungeMaterialValuesUniform;

internal Uniform wearMaterialActiveUniform;
internal Uniform wearMaterialUniform;
internal Uniform wearMaterialValuesUniform;

internal Uniform useCustomColorUniform;
internal Uniform customColorUniform;

internal Uniform irradianceMapUniform;
internal Uniform prefilterMapUniform;
internal Uniform brdfLUTUniform;

internal Uniform cameraPositionUniform;
internal Uniform cameraDirectionUniform;

internal Uniform directionalLightUniform;

extern Config config;

extern Model model;
extern HashMap textures;
extern Cubemap cubemap;
extern GLuint brdfLUT;

internal int32 setMaterialActiveUniform(Uniform *uniform, Material *material);
internal int32 setMaterialUniform(Uniform *uniform, Material *material);
internal int32 setFallbackMaterialUniform(
	Uniform *uniform,
	GLint *textureIndex
);

internal int32 setBindlessTextureUniform(Uniform *uniform, UUID name);
internal int32 setTextureArrayUniform(
	Uniform *uniform,
	uint32 numTextures,
	GLint *textureIndex
);

internal int32 setMaterialValuesUniform(Uniform *uniform, Material *material);

internal void activateFallbackMaterialTextures(
	Material *material,
	GLint *textureIndex
);

internal void activateTexture(UUID name, GLint *textureIndex);

int32 initializeRenderer(void) {
	LOG("Initializing renderer...\n");

	fallbackShaders =
		!checkGLExtension("GL_ARB_bindless_texture") ||
		!checkGLExtension("GL_ARB_gpu_shader_int64");

	if (fallbackShaders) {
		LOG(
			"WARNING: Required OpenGL extensions "
			"(GL_ARB_bindless_texture and GL_ARB_gpu_shader_int64) "
			"are not supported\n"
		);

		LOG("Using fallback shaders\n");
	}

	const char *fragmentShader = PBR_FRAGMENT_SHADER_FILE;
	if (!config.graphicsConfig.pbr) {
		fragmentShader = FRAGMENT_SHADER_FILE;
		fallbackShaders = true;
	} else if (fallbackShaders) {
		fragmentShader = PBR_FALLBACK_FRAGMENT_SHADER_FILE;
	}

	if (createShaderProgram(
		VERTEX_SHADER_FILE,
		NULL,
		NULL,
		NULL,
		fragmentShader,
		NULL,
		&shaderProgram) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "model", UNIFORM_MAT4, &modelUniform) == -1) {
		return -1;
	}

	if (getUniform(shaderProgram, "view", UNIFORM_MAT4, &viewUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"projection",
		UNIFORM_MAT4,
		&projectionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"hasAnimations",
		UNIFORM_BOOL,
		&hasAnimationsUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"boneTransforms",
		UNIFORM_MAT4,
		&boneTransformsUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"materialActive",
		UNIFORM_BOOL,
		&materialActiveUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"material",
		fallbackShaders ? UNIFORM_TEXTURE_2D : UNIFORM_TEXTURE_BINDLESS,
		&materialUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"materialValues",
		UNIFORM_VEC3,
		&materialValuesUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"materialMask",
		UNIFORM_TEXTURE_BINDLESS,
		&materialMaskUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"opacityMaskActive",
		UNIFORM_BOOL,
		&opacityMaskActiveUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"opacityMask",
		fallbackShaders ? UNIFORM_TEXTURE_2D : UNIFORM_TEXTURE_BINDLESS,
		&opacityMaskUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"collectionMaterialActive",
		UNIFORM_BOOL,
		&collectionMaterialActiveUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"collectionMaterial",
		UNIFORM_TEXTURE_BINDLESS,
		&collectionMaterialUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"collectionMaterialValues",
		UNIFORM_VEC3,
		&collectionMaterialValuesUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"grungeMaterialActive",
		UNIFORM_BOOL,
		&grungeMaterialActiveUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"grungeMaterial",
		UNIFORM_TEXTURE_BINDLESS,
		&grungeMaterialUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"grungeMaterialValues",
		UNIFORM_VEC3,
		&grungeMaterialValuesUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"wearMaterialActive",
		UNIFORM_BOOL,
		&wearMaterialActiveUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"wearMaterial",
		UNIFORM_TEXTURE_BINDLESS,
		&wearMaterialUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"wearMaterialValues",
		UNIFORM_VEC3,
		&wearMaterialValuesUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"useCustomColor",
		UNIFORM_BOOL,
		&useCustomColorUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"customColor",
		UNIFORM_VEC3,
		&customColorUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"irradianceMap",
		UNIFORM_TEXTURE_CUBE_MAP,
		&irradianceMapUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"prefilterMap",
		UNIFORM_TEXTURE_CUBE_MAP,
		&prefilterMapUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"brdfLUT",
		UNIFORM_TEXTURE_2D,
		&brdfLUTUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"cameraPosition",
		UNIFORM_VEC3,
		&cameraPositionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"cameraDirection",
		UNIFORM_VEC3,
		&cameraDirectionUniform) == -1) {
		return -1;
	}

	if (getUniform(
		shaderProgram,
		"directionalLight",
		UNIFORM_BOOL,
		&directionalLightUniform) == -1) {
		return -1;
	}

	LOG("Successfully initialized renderer\n");

	return 0;
}

int32 drawModel(Camera *camera, kmVec3 scale, bool wireframe, bool light) {
	if (strlen(model.name.string) == 0) {
		return 0;
	}

	glUseProgram(shaderProgram);

	if (setCameraUniforms(*camera, &viewUniform, &projectionUniform) == -1) {
		return -1;
	}

	GLint textureIndex = 0;
	if (fallbackShaders) {
		setFallbackMaterialUniform(&materialUniform, &textureIndex);
		setUniform(opacityMaskUniform, 1, &textureIndex);
		textureIndex++;
	}

	if (config.graphicsConfig.pbr) {
		setUniform(irradianceMapUniform, 1, &textureIndex);
		textureIndex++;
		setUniform(prefilterMapUniform, 1, &textureIndex);
		textureIndex++;
		setUniform(brdfLUTUniform, 1, &textureIndex);
		textureIndex++;
	}

	kmVec3 cameraPosition;
	kmQuaternionMultiplyVec3(
		&cameraPosition,
		&camera->rotation,
		&camera->position
	);

	kmVec3Add(&cameraPosition, &cameraPosition, &camera->translation);

	if (setUniform(cameraPositionUniform, 1, &cameraPosition) == -1) {
		return -1;
	}

	kmVec3 viewDirection;
	kmQuaternionGetForwardVec3RH(&viewDirection, &camera->rotation);

	if (setUniform(cameraDirectionUniform, 1, &viewDirection) == -1) {
		return -1;
	}

	kmMat4 worldMatrix;
	kmMat4Scaling(
		&worldMatrix,
		scale.x,
		scale.y,
		scale.z
	);

	if (setUniform(modelUniform, 1, &worldMatrix) == -1) {
		return -1;
	}

	bool hasAnimations = model.numAnimations > 0;
	if (setUniform(hasAnimationsUniform, 1, &hasAnimations) == -1) {
		return -1;
	}

	if (hasAnimations) {
		kmMat4 boneMatrices[MAX_BONE_COUNT];
		for (uint32 i = 0; i < MAX_BONE_COUNT; i++) {
			kmMat4Identity(&boneMatrices[i]);
		}

		Skeleton *skeleton = &model.skeleton;
		for (uint32 i = 0; i < skeleton->numBoneOffsets; i++) {
			BoneOffset *boneOffset = &skeleton->boneOffsets[i];
			Transform *jointTransform = hashMapGetData(
				skeleton->joints,
				&boneOffset->name
			);

			Transform globalJointTransform = createTransform(
				&jointTransform->globalPosition,
				&jointTransform->globalRotation,
				&jointTransform->globalScale
			);

			if (jointTransform) {
				Transform finalTransform;
				concatenateTransforms(
					&finalTransform,
					&globalJointTransform,
					&boneOffset->transform
				);

				boneMatrices[i] = composeTransform(&finalTransform);
			}
		}

		setUniform(
			boneTransformsUniform,
			MAX_BONE_COUNT,
			boneMatrices
		);
	}

	if (setUniform(directionalLightUniform, 1, &light) == -1) {
		return -1;
	}

	for (uint32 i = 0; i < model.numSubsets; i++) {
		Subset *subset = &model.subsets[i];
		Mesh *mesh = &subset->mesh;
		Material *material = &subset->material;
		Mask *mask = &subset->mask;

		glBindVertexArray(mesh->vertexArray);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);

		for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
			glEnableVertexAttribArray(j);
		}

		setMaterialActiveUniform(&materialActiveUniform, material);

		bool opacityMaskActive = strlen(model.opacityTexture.string) > 0;
		setUniform(opacityMaskActiveUniform, 1, &opacityMaskActive);

		if (!fallbackShaders) {
			setMaterialActiveUniform(
				&collectionMaterialActiveUniform,
				&mask->collectionMaterial
			);

			setMaterialActiveUniform(
				&grungeMaterialActiveUniform,
				&mask->grungeMaterial
			);

			setMaterialActiveUniform(
				&wearMaterialActiveUniform,
				&mask->wearMaterial
			);
		}

		GLint textureIndex = 0;

		if (fallbackShaders) {
			activateFallbackMaterialTextures(material, &textureIndex);
			activateTexture(model.opacityTexture, &textureIndex);
		}

		if (config.graphicsConfig.pbr) {
			glActiveTexture(GL_TEXTURE0 + textureIndex++);
			glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap.irradianceID);

			glActiveTexture(GL_TEXTURE0 + textureIndex++);
			glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap.prefilterID);

			glActiveTexture(GL_TEXTURE0 + textureIndex++);
			glBindTexture(GL_TEXTURE_2D, brdfLUT);
		}

		if (!fallbackShaders) {
			setMaterialUniform(&materialUniform, material);
			setBindlessTextureUniform(
				&materialMaskUniform,
				model.materialTexture
			);

			setBindlessTextureUniform(
				&opacityMaskUniform,
				model.opacityTexture
			);

			setMaterialUniform(
				&collectionMaterialUniform,
				&mask->collectionMaterial
			);

			setMaterialUniform(&grungeMaterialUniform, &mask->grungeMaterial);
			setMaterialUniform(&wearMaterialUniform, &mask->wearMaterial);
		}

		if (setMaterialValuesUniform(&materialValuesUniform, material) == -1) {
			return -1;
		}

		if (!fallbackShaders) {
			if (setMaterialValuesUniform(
				&collectionMaterialValuesUniform,
				&mask->collectionMaterial) == -1) {
				return -1;
			}

			if (setMaterialValuesUniform(
				&grungeMaterialValuesUniform,
				&mask->grungeMaterial) == -1) {
				return -1;
			}

			if (setMaterialValuesUniform(
				&wearMaterialValuesUniform,
				&mask->wearMaterial) == -1) {
				return -1;
			}
		}

		if (material->doubleSided) {
			glDisable(GL_CULL_FACE);
		}

		setUniform(useCustomColorUniform, 1, &wireframe);

		if (wireframe) {
			setUniform(
				customColorUniform,
				1,
				&config.viewerConfig.wireframeColor
			);

			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			real32 lineWidth;
			glGetFloatv(GL_LINE_WIDTH, &lineWidth);
			glLineWidth(1.0f);

			glDisable(GL_CULL_FACE);

			glDrawElements(
				GL_TRIANGLES,
				mesh->numIndices,
				GL_UNSIGNED_INT,
				NULL
			);

			if (logGLError(
				false,
				"Failed to draw wireframe for subset (%s)",
				subset->name.string) == -1) {
				return -1;
			}

			glLineWidth(lineWidth);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		} else {
			glDrawElements(
				GL_TRIANGLES,
				mesh->numIndices,
				GL_UNSIGNED_INT,
				NULL
			);

			if (logGLError(
				false,
				"Failed to draw subset (%s)",
				subset->name.string) == -1) {
				return -1;
			}
		}

		glEnable(GL_CULL_FACE);

		for (uint8 j = 0; j < 32; j++) {
			glActiveTexture(GL_TEXTURE0 + j);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		for (uint8 j = 0; j < NUM_VERTEX_ATTRIBUTES; j++) {
			glDisableVertexAttribArray(j);
		}

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	glUseProgram(0);

	return 0;
}

int32 setMaterialActiveUniform(Uniform *uniform, Material *material) {
	bool materialActive[MATERIAL_COMPONENT_TYPE_COUNT];
	for (uint8 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		materialActive[i] = strlen(material->components[i].texture.string) > 0;
	}

	if (setUniform(
		*uniform,
		MATERIAL_COMPONENT_TYPE_COUNT,
		materialActive) == -1) {
		return -1;
	}

	return 0;
}

int32 setMaterialUniform(Uniform *uniform, Material *material) {
	GLuint64 textureHandles[MATERIAL_COMPONENT_TYPE_COUNT];
	memset(textureHandles, 0, MATERIAL_COMPONENT_TYPE_COUNT * sizeof(GLuint64));

	for (uint8 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		Texture *texture = hashMapGetData(
			textures,
			&material->components[i].texture
		);

		if (texture) {
			textureHandles[i] = texture->handle;
		}
	}

	return setUniform(*uniform, MATERIAL_COMPONENT_TYPE_COUNT, textureHandles);
}

int32 setFallbackMaterialUniform(Uniform *uniform, GLint *textureIndex) {
	return setTextureArrayUniform(
		uniform,
		MATERIAL_COMPONENT_TYPE_COUNT,
		textureIndex
	);
}

int32 setBindlessTextureUniform(Uniform *uniform, UUID name) {
	GLuint64 textureHandle = 0;

	Texture *texture = hashMapGetData(textures, &name);
	if (texture) {
		textureHandle = texture->handle;
	}

	return setUniform(*uniform, 1, &textureHandle);
}

int32 setTextureArrayUniform(
	Uniform *uniform,
	uint32 numTextures,
	GLint *textureIndex
) {
	GLint textureIndices[numTextures];
	for (uint8 i = 0; i < numTextures; i++) {
		textureIndices[i] = (*textureIndex)++;
	}

	if (setUniform(*uniform, numTextures, textureIndices) == -1) {
		return -1;
	}

	return 0;
}

int32 setMaterialValuesUniform(Uniform *uniform, Material *material) {
	kmVec3 materialValues[MATERIAL_COMPONENT_TYPE_COUNT];
	for (uint8 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		materialValues[i] = material->components[i].value;
	}

	if (setUniform(
		*uniform,
		MATERIAL_COMPONENT_TYPE_COUNT,
		materialValues) == -1) {
		return -1;
	}

	return 0;
}

void activateFallbackMaterialTextures(Material *material, GLint *textureIndex) {
	for (uint8 i = 0; i < MATERIAL_COMPONENT_TYPE_COUNT; i++) {
		activateTexture(material->components[i].texture, textureIndex);
	}
}

void activateTexture(UUID name, GLint *textureIndex) {
	Texture *texture = hashMapGetData(textures, &name);
	if (texture) {
		glActiveTexture(GL_TEXTURE0 + *textureIndex);
		glBindTexture(GL_TEXTURE_2D, texture->id);
	}

	(*textureIndex)++;
}

void shutdownRenderer(void) {
	LOG("Shutting down renderer...\n");

	glDeleteProgram(shaderProgram);

	LOG("Successfully shut down renderer\n");
}