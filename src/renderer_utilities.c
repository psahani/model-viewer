#include "renderer_utilities.h"
#include "file_utilities.h"
#include "log.h"

#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>

internal GLenum getShaderType(ShaderType type);
internal int32 compileShaderFromFile(
	const char *filename,
	ShaderType type,
	Shader *shader
);

internal int32 compileShaderFromSource(
	char *source,
	ShaderType type,
	Shader *shader
);

internal void freeShader(Shader shader);

internal void logShaderInfo(GLuint shader);
internal void logProgramInfo(GLuint program);

GLenum getShaderType(ShaderType type) {
	switch (type) {
		case SHADER_VERTEX:
			return GL_VERTEX_SHADER;
		case SHADER_GEOMETRY:
			return GL_GEOMETRY_SHADER;
		case SHADER_CONTROL:
			return GL_TESS_CONTROL_SHADER;
		case SHADER_EVALUATION:
			return GL_TESS_EVALUATION_SHADER;
		case SHADER_COMPUTE:
			return GL_COMPUTE_SHADER;
		case SHADER_FRAGMENT:
			return GL_FRAGMENT_SHADER;
		default:
			break;
	}

	return GL_INVALID_ENUM;
}

int32 compileShaderFromFile(
	const char *filename,
	ShaderType type,
	Shader *shader
) {
	shader->source = NULL;
	if (!filename) {
		return 0;
	}

	uint64 fileLength;
	char *source = readFile(filename, &fileLength, &logFunction);
	if (source) {
		LOG("Loaded %s\n", filename);
		return compileShaderFromSource(source, type, shader);
	}

	return -1;
}

int32 compileShaderFromSource(char *source, ShaderType type, Shader *shader) {
	GLuint shaderObject = glCreateShader(getShaderType(type));

	glShaderSource(shaderObject, 1, (const GLchar * const *)&source, 0);

	glCompileShader(shaderObject);
	logShaderInfo(shaderObject);

	if (logGLError(true, "Compilation of Shader") == -1) {
		return -1;
	}

	shader->object = shaderObject;
	shader->source = source;

	return 0;
}

void freeShader(Shader shader) {
	if (shader.source) {
		glDeleteShader(shader.object);
		free(shader.source);
		shader.type = SHADER_INVALID;
	}
}

int32 createShaderProgram(
	const char *vertexShader,
	const char *controlShader,
	const char *evaluationShader,
	const char *geometryShader,
	const char *fragmentShader,
	const char *computeShader,
	GLuint *program
) {
	int32 error = 0;

	const char *shaderFiles[SHADER_TYPE_COUNT] = {
		vertexShader,
		controlShader,
		evaluationShader,
		geometryShader,
		fragmentShader,
		computeShader
	};

	Shader shaders[SHADER_TYPE_COUNT];
	memset(shaders, 0, SHADER_TYPE_COUNT * sizeof(Shader));

	for (uint8 i = 0; i < SHADER_TYPE_COUNT; i++) {
		error = compileShaderFromFile(
			shaderFiles[i],
			(ShaderType)i,
			&shaders[i]
		);

		if (error == -1) {
			break;
		}
	}

	if (error != -1) {
		*program = glCreateProgram();

		error = logGLError(true, "Shader Program Creation");

		if (error != -1) {
			for (uint8 i = 0; i < SHADER_TYPE_COUNT; i++) {
				if (shaders[i].source) {
					glAttachShader(*program, shaders[i].object);
				}
			}

			glLinkProgram(*program);
			logProgramInfo(*program);

			error = logGLError(true, "Shader Program Linking");

			// if (error != -1) {
			// 	glValidateProgram(*program);
			// 	logProgramInfo(*program);
			//
			// 	error = logGLError(true, "Shader Program Validation");
			// }
		}
	}

	for (uint8 i = 0; i < SHADER_TYPE_COUNT; i++) {
		freeShader(shaders[i]);
	}

	return error;
}

int32 getUniform(
	GLuint program,
	char *name,
	UniformType type,
	Uniform *uniform
) {
	uniform->type = type;
	uniform->name = name;

	uniform->location = glGetUniformLocation(program, name);

	if (logGLError(false, "Get Uniform (%s)", name) == -1) {
		return -1;
	}

	// LOG("Get Uniform (%s): %d\n", name, uniform->location);

	return 0;
}

int32 setUniform(Uniform uniform, uint32 count, void *data) {
	if (uniform.location > -1) {
		GLint *boolData = NULL;
		switch (uniform.type) {
			case UNIFORM_MAT4:
				glUniformMatrix4fv(uniform.location, count, GL_FALSE, data);
				break;
			case UNIFORM_VEC2:
				glUniform2fv(uniform.location, count, data);
				break;
			case UNIFORM_VEC3:
				glUniform3fv(uniform.location, count, data);
				break;
			case UNIFORM_BOOL:
				boolData = malloc(count * sizeof(GLint));
				for (uint32 i = 0; i < count; i++) {
					boolData[i] = ((bool*)data)[i] ? true : false;
				}

				glUniform1iv(uniform.location, count, boolData);
				free(boolData);

				break;
			case UNIFORM_FLOAT:
				glUniform1fv(uniform.location, count, data);
				break;
			case UNIFORM_INT:
				glUniform1iv(uniform.location, count, data);
				break;
			case UNIFORM_UINT:
				glUniform1uiv(uniform.location, count, data);
				break;
			case UNIFORM_TEXTURE_2D:
			case UNIFORM_TEXTURE_CUBE_MAP:
				glUniform1iv(uniform.location, count, data);
				break;
			case UNIFORM_TEXTURE_BINDLESS:
				glUniformHandleui64vARB(uniform.location, count, data);
			default:
				break;
		}

		if (logGLError(false, "Set Uniform (%s)", uniform.name) == -1) {
			return -1;
		}
	}

	return 0;
}

bool checkGLExtension(const char *extension) {
	int32 numExtensions = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int32 i = 0; i < numExtensions; i++) {
		const GLubyte *ext = glGetStringi(GL_EXTENSIONS, i);
		if (!strcmp((const char *)ext, extension)) {
			return true;
		}
	}

	return false;
}

int32 logGLError(bool logNoError, const char *message, ...) {
	int32 error = 0;

	va_list args;
    va_start(args, message);

	char *messageBuffer = malloc(strlen(message) + 4096);
	vsprintf(messageBuffer, message, args);
    va_end(args);

	GLenum glError = glGetError();
	if (glError != GL_NO_ERROR) {
		error = -1;
	}

	bool logError = error == -1 || (error == 0 && logNoError);
	if (logError) {
		LOG("%s: %s", messageBuffer, gluErrorString(glError));
	}

	free(messageBuffer);

	do {
		glError = glGetError();
		if (glError != GL_NO_ERROR) {
			LOG(", %s", gluErrorString(glError));
		} else {
			break;
		}
	} while (true);

	if (logError) {
		LOG("\n");
	}

	return error;
}

Transform createTransform(
	const kmVec3 *position,
	const kmQuaternion *rotation,
	const kmVec3 *scale
) {
	Transform transform;
	memset(&transform, 0, sizeof(Transform));

	kmVec3Assign(&transform.position, position);
	kmQuaternionAssign(&transform.rotation, rotation);
	kmVec3Assign(&transform.scale, scale);

	transform.parent = NULL;
	transform.numChildren = 0;
	transform.children = NULL;

	return transform;
}

Transform createIdentityTransform(void) {
	Transform transform;
	memset(&transform, 0, sizeof(Transform));

	kmQuaternionIdentity(&transform.rotation);
	kmVec3Fill(&transform.scale, 1.0f, 1.0f, 1.0f);

	transform.parent = NULL;
	transform.numChildren = 0;
	transform.children = NULL;

	return transform;
}

void addChildToTransform(Transform *transform, Transform *child) {
	transform->children = realloc(
		transform->children,
		++transform->numChildren * sizeof(Transform*)
	);

	transform->children[transform->numChildren - 1] = child;
}

kmMat4 composeTransform(const Transform *transform) {
	kmMat4 translationMatrix;
	kmMat4Translation(
		&translationMatrix,
		transform->position.x,
		transform->position.y,
		transform->position.z
	);

	kmQuaternion normalizedRotation;
	kmQuaternionAssign(&normalizedRotation, &transform->rotation);
	kmQuaternionNormalize(&normalizedRotation, &normalizedRotation);

	kmMat4 rotationMatrix;
	kmMat4RotationQuaternion(&rotationMatrix, &normalizedRotation);

	kmMat4 scaleMatrix;
	kmMat4Scaling(
		&scaleMatrix,
		transform->scale.x,
		transform->scale.y,
		transform->scale.z
	);

	kmMat4 transformMatrix;
	kmMat4Multiply(&transformMatrix, &translationMatrix, &rotationMatrix);
	kmMat4Multiply(&transformMatrix, &transformMatrix, &scaleMatrix);

	return transformMatrix;
}

void concatenateTransforms(
	Transform *outTransform,
	const Transform *transformA,
	const Transform *transformB
) {
	kmVec3 *outPosition = &outTransform->position;
	const kmVec3 *positionA = &transformA->position;
	const kmVec3 *positionB = &transformB->position;

	kmQuaternion *outRotation = &outTransform->rotation;
	const kmQuaternion *rotationA = &transformA->rotation;
	const kmQuaternion *rotationB = &transformB->rotation;

	kmVec3 *outScale = &outTransform->scale;
	const kmVec3 *scaleA = &transformA->scale;
	// const kmVec3 *scaleB = &transformB->scale;

	kmVec3Mul(
		outPosition,
		positionB,
		scaleA
	);

	kmQuaternionMultiplyVec3(
		outPosition,
		rotationA,
		outPosition
	);

	kmVec3Add(
		outPosition,
		positionA,
		outPosition
	);

	kmQuaternionMultiply(
		outRotation,
		rotationA,
		rotationB
	);

	kmMat4 matrixA = composeTransform(transformA);
	kmMat4 matrixB = composeTransform(transformB);

	kmMat4 transformMatrix;
	kmMat4Multiply(&transformMatrix, &matrixA, &matrixB);

	kmVec3 xAxis;
	kmVec3Fill(
		&xAxis,
		transformMatrix.mat[0],
		transformMatrix.mat[1],
		transformMatrix.mat[2]
	);

	kmVec3 yAxis;
	kmVec3Fill(
		&yAxis,
		transformMatrix.mat[4],
		transformMatrix.mat[5],
		transformMatrix.mat[6]
	);

	kmVec3 zAxis;
	kmVec3Fill(
		&zAxis,
		transformMatrix.mat[8],
		transformMatrix.mat[9],
		transformMatrix.mat[10]
	);

	kmVec3Fill(
		outScale,
		kmVec3Length(&xAxis),
		kmVec3Length(&yAxis),
		kmVec3Length(&zAxis)
	);
}

kmVec3 transformVertex(const kmVec3 *vertex, const Transform *transform) {
	kmVec3 transformedVertex;

	kmVec3Add(&transformedVertex, vertex, &transform->position);
	kmQuaternionMultiplyVec3(
		&transformedVertex,
		&transform->rotation,
		&transformedVertex
	);
	kmVec3Mul(&transformedVertex, &transformedVertex, &transform->scale);

	return transformedVertex;
}


Transform readTransform(FILE *file) {
	Transform transform = {};
	fread(&transform.position, sizeof(kmVec3), 1, file);
	fread(&transform.rotation, sizeof(kmQuaternion), 1, file);
	fread(&transform.scale, sizeof(kmVec3), 1, file);
	return transform;
}

void freeTransform(Transform *transform) {
	free(transform->children);
}

int32 setCameraUniforms(
	Camera camera,
	Uniform *viewUniform,
	Uniform *projectionUniform
) {
	kmQuaternionMultiplyVec3(
		&camera.position,
		&camera.rotation,
		&camera.position
	);

	kmVec3Add(&camera.position, &camera.position, &camera.translation);

	kmMat3 rotationMatrix;
	kmQuaternionInverse(&camera.rotation, &camera.rotation);
	kmMat3FromRotationQuaternion(&rotationMatrix, &camera.rotation);

	kmMat4 view;
	kmMat4RotationTranslation(&view, &rotationMatrix, &camera.position);
	kmMat4Inverse(&view, &view);

	if (setUniform(*viewUniform, 1, &view) == -1) {
		return -1;
	}

	kmMat4 projection;
	kmMat4PerspectiveProjection(
		&projection,
		camera.fov,
		camera.aspectRatio,
		camera.nearPlane,
		camera.farPlane
	);

	if (setUniform(*projectionUniform, 1, &projection) == -1) {
		return -1;
	}

	return 0;
}

void logShaderInfo(GLuint shader) {
	GLint shaderInfoLogLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);

	if (shaderInfoLogLength > 0) {
		char *shaderInfoLog = malloc(shaderInfoLogLength);

		glGetShaderInfoLog(
			shader,
			shaderInfoLogLength,
			NULL,
			shaderInfoLog
		);

		LOG("%s", shaderInfoLog);
		free(shaderInfoLog);
	}
}

void logProgramInfo(GLuint program) {
	GLint programInfoLogLength;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &programInfoLogLength);

	if (programInfoLogLength > 0) {
		char *programInfoLog = malloc(programInfoLogLength);

		glGetProgramInfoLog(
			program,
			programInfoLogLength,
			NULL,
			programInfoLog
		);

		LOG("%s", programInfoLog);
		free(programInfoLog);
	}
}